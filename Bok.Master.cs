﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.Rugby.BokSmart.Web.General;

namespace SuperSport.Rugby.BokSmart.Web
{
    public partial class Bok : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NavMenu1.SiteID = SiteConfig.NavigationSiteID;
            NavMenu1.DataBind();

            ucSaRugbyNews.SiteID = int.Parse(ConfigHelper.GetApplicationConfigSetting("SARugbySiteId"));
            ucSaRugbyNews.DataBind();

            ucRightAd1.SiteID = SiteConfig.SiteID;
            ucRightAd1.ContentType = "RightAd1";
            ucRightAd1.DataBind();

            ucRightAd2.SiteID = SiteConfig.SiteID;
            ucRightAd2.ContentType = "RightAd2";
            ucRightAd2.DataBind();

            ucRightAd3.SiteID = SiteConfig.SiteID;
            ucRightAd3.ContentType = "RightAd3";
            ucRightAd3.DataBind();

            ucRightAd4.SiteID = SiteConfig.SiteID;
            ucRightAd4.ContentType = "RightAd4";
            ucRightAd4.DataBind();

            ucRightAd5.SiteID = SiteConfig.SiteID;
            ucRightAd5.ContentType = "RightAd5";
            ucRightAd5.DataBind();

            ucRightAd6.SiteID = SiteConfig.SiteID;
            ucRightAd6.ContentType = "RightAd6";
            ucRightAd6.DataBind();
        }
    }
}