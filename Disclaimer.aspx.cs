﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;

namespace SuperSport.Rugby.BokSmart.Web
{
    public partial class Disclaimer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ucTopBannerAd.SiteID = SiteConfig.SiteID;
            ucTopBannerAd.ContentType = "TopBannerAd";
            ucTopBannerAd.DataBind();

            ucDisclaimer.SiteID = SiteConfig.SiteID;
            ucDisclaimer.ContentType = "Disclaimer";
            ucDisclaimer.DataBind();
        }
    }
}