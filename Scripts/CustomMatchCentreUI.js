﻿// -- Utility Functions --
// Enable function defaults
Function.prototype.defaults = function () {
    var _f = this;
    var _a = Array(Math.max(_f.length - arguments.length, 0)).concat(
        Array.prototype.slice.apply(arguments));
    return function () {
        return _f.apply(_f, Array.prototype.slice.apply(arguments).concat(
            _a.slice(arguments.length, _a.length)));
    }
};

(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.pager", {
        // These options will be used as defaults
        options: {
            duration: 'normal',
            animation: 'leftToRightSlide',
            displayIndex: 1,
            _animating: 0,
            _displayCount: 0
        },
        _createPagerWrapper: function () {
            var widget = this;
            var element = this.element[0];
            // Relatively position main element and hide overflowing content.
            $(element).css({ 'overflow': 'hidden' });
            $(element).addClass('mc-ui-posrel');
            // Mark the list of items to page with class.
            // Wrap it in a pageable container.
            var pagerElements =
                $(element).children('div')
                    .addClass('mc-ui-pager-item-container')
                    .wrapAll('<div class="mc-ui-pager-container" />');
            widget.options._displayCount = pagerElements.length;
            $('div.mc-ui-pager-container', element)
                .after('<span class="mc-ui-pager-next">&nbsp;</span>')
                .after('<span class="mc-ui-pager-prev">&nbsp;</span>');
        },
        _createLeftToRightSlide: function () {
            var widget = this;
            var element = this.element[0];
            this._createPagerWrapper();

            // Setup all pager items to float next to each other.
            // Adjust pageable container width for left to right sliding.
            var pagerContainerWidth = 0;
            $('div.mc-ui-pager-container div.mc-ui-pager-item-container', element)
                .addClass('mc-ui-float-l')
                .each(
                    function () {
                        pagerContainerWidth += $(this).outerWidth(true);
                    }
                );
            $('div.mc-ui-pager-container', element).css('width', pagerContainerWidth + 'px');
            // Position pageable container for displaying first page.
            $('div.mc-ui-pager-container', element).addClass('mc-ui-posabs-tl');
            // Adjust height of main element according to first page.
            widget.options.displayIndex = 1;
            var displayHeight = $('div.mc-ui-pager-item-container:nth-child(' + widget.options.displayIndex + ')', element).outerHeight(true);
            $(element).css('height', displayHeight + 'px');
            $(element).mouseenter(function () { $(this).addClass('ui-state-hover') });
            $(element).mouseleave(function () { $(this).removeClass('ui-state-hover') });
            // Setup previous and next clickable elements.
            $('span.mc-ui-pager-prev', element).addClass('mc-ui-posabs-tl').unbind('click').click(function (event) { event.preventDefault(); widget._slideRight() });
            $('span.mc-ui-pager-next', element).addClass('mc-ui-posabs-tr').unbind('click').click(function (event) { event.preventDefault(); widget._slideLeft() });
            if (widget.options._displayCount <= 1) {
                $('span.mc-ui-pager-prev', element).hide();
                $('span.mc-ui-pager-next', element).hide();
            }
            if (widget.options.displayIndex >= widget.options._displayCount) {
                $('span.mc-ui-pager-next', element).hide();
            }
            if (widget.options.displayIndex <= 1) {
                $('span.mc-ui-pager-prev', element).hide();
            }
        },
        _slideLeft: function () {
            var widget = this;
            var element = this.element[0];
            if (widget.options._animating > 0)
                return false;
            widget.options._animating++;
            // Get current left offset.
            var displayLeft = $('div.mc-ui-pager-container', element).position().left;
            // Get current left offset slide delta.
            var slideWidth = $('div.mc-ui-pager-item-container:nth-child(' + widget.options.displayIndex + ')', element).outerWidth(true);
            // Adjust display index for one page right and adjust paging buttons.
            widget.options.displayIndex++;
            if (widget.options.displayIndex >= widget.options._displayCount) {
                $('span.mc-ui-pager-next', element).fadeOut(widget.options.duration);
            }
            if (widget.options.displayIndex > 1) {
                $('span.mc-ui-pager-prev', element).fadeIn(widget.options.duration);
            }
            // Adjust display height of main element according to the next page to display.
            var displayHeight = $('div.mc-ui-pager-item-container:nth-child(' + widget.options.displayIndex + ')', element).outerHeight(true);
            widget.options._animating++;
            $(element).animate({ 'height': displayHeight + 'px' }, widget.options.duration, function () { widget.options._animating--; });
            // Slide pageable container left.
            widget.options._animating++;
            $('div.mc-ui-pager-container', element).animate({ 'left': (displayLeft - slideWidth) + 'px' }, widget.options.duration, function () { widget.options._animating--; });
            widget.options._animating--;
        },
        _slideRight: function () {
            var widget = this;
            var element = this.element[0];
            if (widget.options._animating > 0)
                return false;
            widget.options._animating++;
            // Get current left offset.
            var displayLeft = $('div.mc-ui-pager-container', element).position().left;
            // Adjust display index for one page right.
            widget.options.displayIndex--;
            // Get current left offset slide delta.
            var slideWidth = $('div.mc-ui-pager-item-container:nth-child(' + widget.options.displayIndex + ')', element).outerWidth(true);
            // Adjust paging buttons.
            if (widget.options.displayIndex <= 1) {
                $('span.mc-ui-pager-prev', element).fadeOut(widget.options.duration);
            }
            if (widget.options.displayIndex < widget.options._displayCount) {
                $('span.mc-ui-pager-next', element).fadeIn(widget.options.duration);
            }
            // Adjust display height of main element according to the next page to display.
            var displayHeight = $('div.mc-ui-pager-item-container:nth-child(' + widget.options.displayIndex + ')', element).outerHeight(true);
            widget.options._animating++;
            $(element).animate({ 'height': displayHeight + 'px' }, widget.options.duration, function () { widget.options._animating--; });
            // Slide pageable container left.
            widget.options._animating++;
            $('div.mc-ui-pager-container', element).animate({ 'left': (displayLeft + slideWidth) + 'px' }, widget.options.duration, function () { widget.options._animating--; });
            widget.options._animating--;
        },
        _create: function () {
            // The _create method is where you set up the widget
            var widget = this;
            var element = this.element[0];

            switch (widget.options.animation) {
                case 'leftToRightSlide':
                default:
                    widget._createLeftToRightSlide();
                    break;
            }

        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });
})(jQuery);


(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.pagerii", {
        // These options will be used as defaults
        options: {
            duration: 'normal',
            page: 1,
            countPerPage: 0,
            _pages: [],
            _pagecount: 0,
            _animating: 0,
            routerURL: '',
            fullClassName: '',
            pageMethodName: 'Page',
            additionalParams: []
        },
        _createPagerWrapper: function () {
            var widget = this;
            var element = this.element[0];
            // Relatively position main element and hide overflowing content.
            // The main element will be the view port.
            $(element).css({ 'overflow': 'hidden' });
            $(element).addClass('mc-ui-posrel');

            // NOTE: All divs that are direct children of the main element are 
            // considered individual pages.

            // - Wrap the list of items to page in a pageable container.
            var pagerElements =
                $(element).children('div')
                    .wrapAll('<div class="mc-ui-pager-container" />');

            // - Add previous and next buttons to the widget.
            $('div.mc-ui-pager-container', element)
                .after('<span class="mc-ui-pager-next">&nbsp;</span>')
                .after('<span class="mc-ui-pager-prev">&nbsp;</span>');
        },
        _initPager: function () {
            var widget = this;
            var element = this.element[0];

            // Reset cached pages.
            widget.options._pages = [];
            widget.options._pagecount = 0;

            // - Mark the list of items to page with a class "mc-ui-pager-item-container". 
            var pagerElements =
                $('.mc-ui-pager-container', element).children('div')
                    .addClass('mc-ui-pager-item-container')
                    .each(function () {
                        widget.options._pages[++widget.options._pagecount] = $(this);
                    });

            // - Setup interaction states
            $(element).unbind('mouseenter').mouseenter(function () { $(this).addClass('ui-state-hover') });
            $(element).unbind('mouseleave').mouseleave(function () { $(this).removeClass('ui-state-hover') });
        },
        _initHorizontalPager: function () {
            var widget = this;
            var element = this.element[0];

            // - Setup all pager items to float next to each other.
            // - Adjust pageable container width for horizontal viewing.
            var pagerContainerWidth = 0;
            $('div.mc-ui-pager-container div.mc-ui-pager-item-container', element)
                .addClass('mc-ui-float-l')
                .each(function () {
                    pagerContainerWidth += $(this).outerWidth(true);
                });
            $('div.mc-ui-pager-container', element).css('width', pagerContainerWidth + 'px');

            // - Position pageable container for displaying selected page.
            var selectedPage = widget.options._pages[widget.options.page];
            var selectedPageLeft = $(selectedPage).position().left;
            $('div.mc-ui-pager-container', element).addClass('mc-ui-posabs');
            $('div.mc-ui-pager-container', element).css('left', '-' + selectedPageLeft);

            // - Adjust height of main element according to first page.
            var displayHeight = $('div.mc-ui-pager-item-container:nth-child(' + widget.options.page + ')', element).outerHeight(true);
            $(element).css('height', displayHeight + 'px');

            // - Setup previous and next clickable elements.
            $('span.mc-ui-pager-prev', element).addClass('mc-ui-posabs-tl').unbind('click').click(function (event) { event.preventDefault(); widget._prevClick() });
            $('span.mc-ui-pager-next', element).addClass('mc-ui-posabs-tr').unbind('click').click(function (event) { event.preventDefault(); widget._nextClick() });

            // - Initialize display of previous and next buttons.
            if ($(selectedPage).hasClass('mc-ui-final')) {
                $('span.mc-ui-pager-next', element).hide();
            }
            else {
                // - Cache next page so long if possible.
                widget._cacheNext();
            }

            if (widget.options.page <= 1) {
                $('span.mc-ui-pager-prev', element).hide();
            }
        },
        _cacheNext: function () {
            var widget = this;
            var element = this.element[0];

            if (undefined != widget.options._pages[widget.options.page + 1])
                return;

            widget.options._animating++;
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: widget.options.routerURL,
                data:
            "{" +
            "fullClassName:'" + widget.options.fullClassName + "'," +
            "methodName:'" + widget.options.pageMethodName + "'," +
            "parameters:['" + (widget.options.page + 1) + "','" + widget.options.countPerPage + (widget.options.additionalParams.length > 0 ? "','" : "") + widget.options.additionalParams.join("','") + "']}",
                success: function (result, textStatus, jqXHR) {
                    // Receive new contents.
                    var newElement = $(result.d)
                .addClass('mc-ui-pager-item-container')
                .addClass('mc-ui-float-l')
                .each(function () {
                    widget.options._pages[++widget.options._pagecount] = $(this);
                });

                    var currentPage = widget.options._pages[widget.options.page];

                    // Insert new contents in DOM.
                    $(currentPage).after($(newElement));

                    var pagerContainerWidth = $('div.mc-ui-pager-container', element).outerWidth(true);

                    $(newElement)
                .each(function () {
                    pagerContainerWidth += $(this).outerWidth(true);
                });

                    $('div.mc-ui-pager-container', element).css('width', pagerContainerWidth + 'px');

                    if (widget.options.success) {
                        widget.options.success($(newElement), result, textStatus, jqXHR);
                    }
                },
                complete: function (jqXHR, textStatus) {
                    widget.options._animating--;
                }
            });
        },
        _nextClick: function () {
            var widget = this;
            var element = this.element[0];

            if (widget.options._animating > 0)
                return false;

            widget.options._animating++;

            widget._slideLeft();

            widget.options._animating--;
        },
        _slideLeft: function () {
            var widget = this;
            var element = this.element[0];

            // - Get current left offset.
            var currentOffset = $('div.mc-ui-pager-container', element).position().left;
            // - Get current left offset slide delta.
            var currentPage = widget.options._pages[widget.options.page];
            var currentPageWidth = $(currentPage).outerWidth(true);
            var slideDelta = currentPageWidth;

            // Adjust display height of main element according to the next page to display.
            var displayHeight = $('div.mc-ui-pager-item-container:nth-child(' + (widget.options.page + 1) + ')', element).outerHeight(true);
            widget.options._animating++;
            $(element).animate({ 'height': displayHeight + 'px' }, widget.options.duration, function () { widget.options._animating--; });
            // Slide pageable container left.
            widget.options._animating++;
            $('div.mc-ui-pager-container', element).animate(
                { 'left': (currentOffset - slideDelta) + 'px' },
                widget.options.duration,
                function () {
                    widget.options._animating--;

                    // - Load the next page with AJAX if necessary
                    widget._cacheNext();
                }
            );

            // - Adjust display index for one page right.
            widget.options.page++;

            var nextPage = widget.options._pages[widget.options.page];

            // - Adjust the paging buttons.
            if ($(nextPage).hasClass('mc-ui-final')) {
                $('span.mc-ui-pager-next', element).fadeOut(widget.options.duration);
            }
            if (widget.options.page > 1) {
                $('span.mc-ui-pager-prev', element).fadeIn(widget.options.duration);
            }
        },
        _prevClick: function () {
            var widget = this;
            var element = this.element[0];

            if (widget.options._animating > 0)
                return false;

            widget.options._animating++;

            widget._slideRight();

            widget.options._animating--;

            // - Load the next page with AJAX if necessary
            //if (undefined == widget.options._pages[widget.options.page + 1])
            //{
            //    widget._cacheNext();
            //}
        },
        _slideRight: function () {
            var widget = this;
            var element = this.element[0];

            // - Get current left offset.
            var currentOffset = $('div.mc-ui-pager-container', element).position().left;
            // - Get current left offset slide delta.
            var currentPage = widget.options._pages[widget.options.page];
            var currentPageWidth = $(currentPage).outerWidth(true);
            var slideDelta = currentPageWidth;


            // Adjust display height of main element according to the next page to display.
            var displayHeight = $('div.mc-ui-pager-item-container:nth-child(' + (widget.options.page - 1) + ')', element).outerHeight(true);
            widget.options._animating++;
            $(element).animate({ 'height': displayHeight + 'px' }, widget.options.duration, function () { widget.options._animating--; });
            // Slide pageable container left.
            widget.options._animating++;
            $('div.mc-ui-pager-container', element).animate({ 'left': (currentOffset + slideDelta) + 'px' }, widget.options.duration, function () { widget.options._animating--; });

            // - Adjust display index for one page left.
            widget.options.page--;

            var nextPage = widget.options._pages[widget.options.page];

            // - Adjust the paging buttons.
            if (!$(nextPage).hasClass('mc-ui-final')) {
                $('span.mc-ui-pager-next', element).fadeIn(widget.options.duration);
            }
            if (widget.options.page <= 1) {
                $('span.mc-ui-pager-prev', element).fadeOut(widget.options.duration);
            }
        },
        _create: function () {
            // The _create method is where you set up the widget
            var widget = this;
            var element = this.element[0];

            widget._createPagerWrapper();
            /*
            switch (widget.options.animation) {
            case 'leftToRightSlide':
            default:
            widget._createLeftToRightSlide();
            break;
            }
            */
        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];

            widget._initPager();
            widget._initHorizontalPager();
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });
})(jQuery);


// -- Navigation JQuery Extensions --
(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.navigation", {
        // These options will be used as defaults
        options: {
            duration: 'fast',
            animation: // Animation and display parameters for displaying next tier.
                [
                    { slide: 'vertical' }, // Second Tier from First Tier
                    {slide: 'vertical' }, // Third Tier from Second Tier
                    {slide: 'horisontal'} // Fourth to Nth Tier from third to (N-1)th Tier.
                ],
            overridecss: true,
            slideopen: 200,
            slideclose: 50,
            hoverintent: 100
        },
        _create: function () {
            // The _create method is where you set up the widget
            var element = this.element[0];
        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];

            // Set default positioning.
            $(element).addClass('mc-ui-posrel');
            $(element).css('position', 'relative');

            // Hide 2nd to nth tier menus
            $('> ul > li ul', element).hide();

            // Show Selected Sub Menu
            $('> ul li', element).hover(
                function () {
                    var hoverTarget = this;
                    $(hoverTarget).addClass('ui-state-hover');

                    // Get tier index.
                    var tierIdx = $(hoverTarget).parents('li.ui-state-hover', element).length;

                    // Get display and animation parameters.
                    var animation = widget.options.animation[Math.min(tierIdx, widget.options.animation.length)];
                    var slide = animation.slide;
                    var position = animation.position;

                    // Set the position of the target and Sub Menu
                    if (widget.options.overridecss) {
                        if (position != undefined)
                            $(hoverTarget).css({ position: position });

                        if ('horisontal' == slide) {
                            $('> ul', hoverTarget).css({ position: 'absolute', top: '0px', left: '100%' });
                        }
                        else if ('vertical' == slide) {
                            $('> ul', hoverTarget).css({ position: 'absolute', top: '100%', left: '0px' });
                        }
                    }

                    // Trigger animation
                    setTimeout(function () {
                        if (!$(hoverTarget).hasClass('ui-state-hover'))
                            return;

                        // Hide the nth tier menus
                        $('> ul > li ul', hoverTarget).hide();
                        if ('horisontal' == slide) {
                            $('> ul', hoverTarget).show(widget.options.slideopen);
                        }
                        else if ('vertical' == slide) {
                            $('> ul', hoverTarget).slideDown(widget.options.slideopen);
                        }
                    }, widget.options.hoverintent);
                },
                function () {
                    var hoverTarget = this;
                    $(hoverTarget).removeClass('ui-state-hover');

                    // Get tier index.
                    var tierIdx = $(hoverTarget).parents('li.ui-state-hover', element).length;

                    // Get display and animation parameters.
                    var animation = widget.options.animation[Math.min(tierIdx, widget.options.animation.length)];
                    var slide = animation.slide;

                    // Trigger animation
                    if ('horisontal' == slide) {
                        $('> ul', hoverTarget).hide(widget.options.slideclose);
                    }
                    else if ('vertical' == slide) {
                        $('> ul', hoverTarget).slideUp(widget.options.slideclose);
                    }
                }
            );
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });
})(jQuery);


// -- News Browsing JQuery Extensions --
(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.newsbrowser", {
        // These options will be used as defaults
        options: {
            duration: 'normal',
            onswitch: function () {
            },
            browseBehaviour: 1,
            browseEffect: 1,
            browseAutoCycle: false
        },
        _create: function () {
            // The _create method is where you set up the widget
            var element = this.element[0];
        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];

            // Set classes on odd and even elements
            /*
            $('div.featureitem', element)
                .filter(':odd')
                .addClass('mc-ui-odd');
            $('li.featureitem', element)
                .filter(':even')
                .addClass('mc-ui-even');
             */

            // Set the default display classes.
//            $('li.mc-ui-news-list-item .mc-ui-news-item-container', element)
//                .addClass('ui-state-default');

            // Get the first li to display and add the active class to indicate the selected li.
            var firstDiv = $('div.featureitem', element).first();
            var leftBlock = $('div.block300_left', element).first();
            
            try
            {
                $('a', leftBlock).replaceWith($('#hdnImageUrl', firstDiv)[0].innerHTML);
            }catch (e){ }

            // make sure that each item is highlighted on hover
            $('div.featureitem', element).each(function(index) {
                    $(this).hover(function() {
                            //make sure the first item is deselected if
                            //we are not hovering over that item
                            if(index != 0){
                                firstDiv.removeClass('featureselected');
                                firstDiv.addClass('featureitem');
                            }
                            $(this).removeClass('featureitem');
                            $(this).addClass('featureselected');
                            $(this).css('cursor', 'pointer');

                            $('a', leftBlock)
                                .replaceWith($('#hdnImageUrl', this)[0].innerHTML);
                                //.clone();
                            //$('#hdnImageUrl', this)[0].innerHTML
                        },
                        function() {
                            $(this).removeClass('featureselected');
                            $(this).addClass('featureitem');
                            $(this).css('cursor', 'auto');
                        }
                    );
                }
            );

            // Highlight the first item
            firstDiv.removeClass('featureitem');
            firstDiv.addClass('featureselected');

            widget._setupBrowseBehaviour();

        },
        _setupBrowseBehaviour: function () {
            var widget = this;
            var element = this.element[0];

            $('.mc-ui-news-list-item .mc-ui-news-item-container', element).unbind('hover').hover(
                function () {
                    var hoverTarget = this;
                    $('.mc-ui-news-list-item .mc-ui-news-item-container', element).removeClass('ui-state-hover');
                    $(hoverTarget).addClass('ui-state-hover');
                },
                function () {
                    var hoverTarget = this;
                    $(hoverTarget).removeClass('ui-state-hover');
                }
            );

            if ($.matchcentreui.newsbrowser.prototype._browseBehaviourEnum.switchOnClick == widget.options.browseBehaviour) {
                widget._setupClickBrowseBehaviour();
            } else {
                widget._setupHoverBrowseBehaviour();
            }
        },
        _setupClickBrowseBehaviour: function () {
            var widget = this;
            var element = this.element[0];

            $('.mc-ui-news-list-item .mc-ui-news-item-container', element).unbind('click').click(
                function () {
                    var clickTarget = this;
                    $('.mc-ui-news-list-item .mc-ui-news-item-container', element)
                        .switchClass('ui-state-active', 'ui-state-default', widget.options.duration);

                    $(clickTarget).closest('.mc-ui-news-item-container')
                        .switchClass('ui-state-default', 'ui-state-active', widget.options.duration);

                }
            );
        },
        _setupHoverBrowseBehaviour: function () {
            var widget = this;
            var element = this.element[0];

            $('.mc-ui-news-list-item .mc-ui-news-item-container', element).hover(
                function () {
                    var hoverTarget = this;

                    setTimeout(function () {
                        var hovHeadline =
                        $.trim($(hoverTarget)
                            .closest('.mc-ui-news-item-container')
                            .find('.mc-ui-news-item-headline-non-ellipsis')
                            .html());

                        var curHeadline =
                         $.trim($('.mc-ui-news-item-view-container .mc-ui-news-item-container .mc-ui-news-item-headline-non-ellipsis', element)
                            .html());
                        if (hovHeadline == curHeadline) {
                            return true;
                        }

                        if (!$(hoverTarget).hasClass('ui-state-hover'))
                            return true;

                        $('.mc-ui-news-list-item .mc-ui-news-item-container', element)
                            .switchClass('ui-state-active', 'ui-state-default', 0);

                        $(hoverTarget).closest('.mc-ui-news-item-container')
                            .switchClass('ui-state-default', 'ui-state-active', 0);

                        widget._execBrowseEffect(hoverTarget);
                    }, 200);
                },
                function () {
                    var hoverTarget = this;
                    $(hoverTarget).removeClass('ui-state-hover');
                }
            );
        },
        _execBrowseEffect: function (target) {
            var widget = this;
            var element = this.element[0];

            if ($.matchcentreui.newsbrowser.prototype._browseEffectEnum.slideLeft == widget.options.browseEffect) {
                widget._execEffectSlideLeftToggle(target);
            } else {
                widget._execEffectFadeToggle(target);
            }
        },
        _execEffectFadeToggle: function (target) {
            var widget = this;
            var element = this.element[0];
            $('.mc-ui-news-item-view-container .mc-ui-news-item-container', element).fadeOut(
                widget.options.duration,
                function () {
                    $('.mc-ui-news-item-view-container .mc-ui-news-item-container', element)
                            .after(
                                $(target)
                                    .closest('.mc-ui-news-item-container')
                                    .clone()
                                    .fadeIn(widget.options.duration)
                            )
                            .remove();
                    widget.options.onswitch();
                });
        },
        _execEffectSlideLeftToggle: function (target) {
            var widget = this;
            var element = this.element[0];
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });

    $.matchcentreui.newsbrowser.prototype._browseBehaviourEnum =
        {
            switchOnHover: 1,
            switchOnClick: 2
        };
    $.matchcentreui.newsbrowser.prototype._browseEffectEnum =
        {
            fadeToggle: 1,
            slideLeft: 2,
            slideRight: 3,
            slideUp: 4,
            slideDown: 5,
            slideToggle: 6
        };
})(jQuery);



// -- Fixture List JQuery Extensions --
(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.fixtureslist", {
        // These options will be used as defaults
        options: {
            duration: 'fast',
            animation: 'slide',
            displayIndex: 1
        },
        _create: function () {
            // The _create method is where you set up the widget
            var widget = this;
            var element = this.element[0];

            if ($(element).hasClass('mc-ui-compact'))
                return;

            switch (widget.options.animation) {
                case 'none':
                    break;
                case 'slide':
                default:
                    $(element).pager();
                    break;
            }

        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];

            // Set classes on odd and even elements
            $('.mc-ui-fixture-item-container', element)
                .filter(':odd')
                .addClass('mc-ui-odd');
            $('.mc-ui-fixture-item-container', element)
                .filter(':even')
                .addClass('mc-ui-even');

            // Set the default display classes.
            $('.mc-ui-fixture-item-container', element)
                .addClass('ui-state-default');
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });
})(jQuery);


// -- Result List JQuery Extensions --
(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.resultslist", {
        // These options will be used as defaults
        options: {
            duration: 'fast',
            animation: 'slide',
            displayIndex: 1
        },
        _create: function () {
            // The _create method is where you set up the widget
            var widget = this;
            var element = this.element[0];

            if ($(element).hasClass('mc-ui-compact'))
                return;

            switch (widget.options.animation) {
                case 'none':
                    break;
                case 'slide':
                default:
                    $(element).pager();
                    break;
            }

        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];

            // Set classes on odd and even elements
            $('.mc-ui-result-item-container', element)
                .filter(':odd')
                .addClass('mc-ui-odd');
            $('.mc-ui-result-item-container', element)
                .filter(':even')
                .addClass('mc-ui-even');

            // Set the default display classes.
            $('.mc-ui-result-item-container', element)
                .addClass('ui-state-default');
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });
})(jQuery);


// -- Logs List JQuery Extensions --
(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.logs", {
        // These options will be used as defaults
        options: {
        },
        _create: function () {
            // The _create method is where you set up the widget
            var widget = this;
            var element = this.element[0];
        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];

            // Set classes on odd and even elements
            $('.mc-ui-log-item-container', element)
                .filter(':odd')
                .addClass('mc-ui-odd');
            $('.mc-ui-log-item-container', element)
                .filter(':even')
                .addClass('mc-ui-even');

            // Set the default display classes.
            $('.mc-ui-log-item-container', element)
                .addClass('ui-state-default');
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });
})(jQuery);



// -- Gallery Browser JQuery Extensions --
(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.galleriesbrowser", {
        // These options will be used as defaults
        options: {
            animation: 'slide',
            isgallerypopup: false
        },
        _create: function () {
            // The _create method is where you set up the widget
        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];

            if ($(element).hasClass('mc-ui-compact'))
                return;

            switch (widget.options.animation) {
                case 'slide':
                default:
                    if ('undefined' != typeof widget.options.pagerii) {
                        widget.options.pagerii.success =
                            function (newelement, data, textStatus, jqXHR) {
                                if (widget.options.isgallerypopup) {
                                    $('.mc-ui-gallery-item a', $(newelement)).facebox();
                                }
                                $('.mc-ui-gallery-item', $(newelement)).filter(':even').addClass('mc-ui-even');
                                $('.mc-ui-gallery-item', $(newelement)).filter(':odd').addClass('mc-ui-odd');
                                $('.mc-ui-gallery-item .mc-ui-gallery-item-headline-ellipsis', $(newelement)).dotdotdot({ watch: true });
                                $('.mc-ui-gallery-item .mc-ui-gallery-item-description-ellipsis', $(newelement)).dotdotdot({ watch: true });
                            };
                        $(element).pagerii(widget.options.pagerii);
                    } else {
                        $(element).pager();
                    }
                    break;
            }

            if (widget.options.isgallerypopup) {
                $('.mc-ui-gallery-item a', element).facebox();
            }
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });
})(jQuery);




(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.gallerybrowser", {
        // These options will be used as defaults
        options: {
        },
        _create: function () {
            // The _create method is where you set up the widget
        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];
            $(element).galleria({ debug: false });
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });
})(jQuery);




// -- Featured Poll JQuery Extensions --
(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.featuredpoll", {
        // These options will be used as defaults
        options: {
            routerURL: '',
            siteID: 0,
            pollCategory: '',
            fullClassName: '',
            viewMethodName: 'View',
            returnMethodName: 'ReturnToVote',
            voteMethodName: 'Vote'
        },
        _create: function () {
            // The _create method is where you set up the widget
            var widget = this;
            var element = this.element[0];
            this._setupViewClick(widget, element);
            this._setupReturnToVoteClick(widget, element);
            this._setupVoteClick(widget, element);

            $('.mc-ui-poll-vote-button', element).button();
            $('.mc-ui-poll-view-button', element).button();
            $('.mc-ui-poll-return-button', element).button();
        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        _setupViewClick: function (widget, element) {
            $('.mc-ui-poll-view-button', element)
                .unbind('click')
                .click(
                    function (event) {
                        event.preventDefault();
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: widget.options.routerURL,
                            data:
            "{" +
            "fullClassName:'" + widget.options.fullClassName + "'," +
            "methodName:'" + widget.options.viewMethodName + "'," +
            "parameters:['" + widget.options.siteID + "','" + widget.options.pollCategory + "']}",
                            success: function (result) {
                                // Receive new contents.
                                var newElement = $(result.d);
                                // Insert new contents in DOM.
                                $(element).before($(newElement));
                                // Store options and id before removing old contents.
                                var elementId = $(element).attr('id');
                                var options = widget.options;
                                $(element).remove();
                                // Setup new featured poll.
                                $(newElement).attr('id', elementId);
                                $(newElement).featuredpoll(options);
                            }
                        });
                        return false;
                    }
                );
        },
        _setupReturnToVoteClick: function (widget, element) {
            $('.mc-ui-poll-return-button', element)
                .unbind('click')
                .click(
                    function (event) {
                        event.preventDefault();
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: widget.options.routerURL,
                            data:
            "{" +
            "fullClassName:'" + widget.options.fullClassName + "'," +
            "methodName:'" + widget.options.returnMethodName + "'," +
            "parameters:['" + widget.options.siteID + "','" + widget.options.pollCategory + "']}",
                            success: function (result) {
                                // Receive new contents.
                                var newElement = $(result.d);
                                // Insert new contents in DOM.
                                $(element).before($(newElement));
                                // Store options and id before removing old contents.
                                var elementId = $(element).attr('id');
                                var options = widget.options;
                                $(element).remove();
                                // Setup new featured poll.
                                $(newElement).attr('id', elementId);
                                $(newElement).featuredpoll(options);
                            }
                        });
                        return false;
                    }
                );
        },
        _setupVoteClick: function (widget, element) {
            $('.mc-ui-poll-vote-button', element)
                .unbind('click')
                .click(
                    function (event) {
                        event.preventDefault();
                        var pollID = $('input[id$="hPollID"]').val();
                        var answerID = $('input[name$="radAnswers"]:checked').val();
                        if (answerID == undefined)
                            return false;
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: widget.options.routerURL,
                            data:
            "{" +
            "fullClassName:'" + widget.options.fullClassName + "'," +
            "methodName:'" + widget.options.voteMethodName + "'," +
            "parameters:['" + widget.options.siteID + "','" + widget.options.pollCategory + "','" + pollID + "','" + answerID + "']}",
                            success: function (result) {
                                // Receive new contents.
                                var newElement = $(result.d);
                                // Insert new contents in DOM.
                                $(element).before($(newElement));
                                // Store options and id before removing old contents.
                                var elementId = $(element).attr('id');
                                var options = widget.options;
                                $(element).remove();
                                // Setup new featured poll.
                                $(newElement).attr('id', elementId);
                                $(newElement).featuredpoll(options);
                            }
                        });
                        return false;
                    }
                );
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });
})(jQuery);


(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.squadbrowser", {
        // These options will be used as defaults
        options:
        {
            dynamic: true
        },
        _create: function () {
            // The _create method is where you set up the widget
        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];
            $('.mc-ui-squad-position-group', element).each(
                function () {
                    $('.mc-ui-squad-member-container', $(this))
                        .filter(':odd')
                        .addClass('mc-ui-odd');
                    $('.mc-ui-squad-member-container', $(this))
                        .filter(':even')
                        .addClass('mc-ui-even');
                }
            );

            if (widget.options.dynamic) {
                $(element).accordion();
            }
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });
})(jQuery);

(function ($) {
    $(document).bind('afterClose.facebox', function () {
        $('#facebox .content').empty();
    });
})(jQuery);

/*
(function ($) {
// The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
$.widget("matchcentreui.squadbrowser", {
// These options will be used as defaults
options:
{
dynamic: true
},
_create: function () {
// The _create method is where you set up the widget
},
_init: function () {
// The _init method is where you initialise or re-initialise the widget
var widget = this;
var element = this.element[0];
$('.mc-ui-squad-position-group', element).each(
function () {
$('.mc-ui-squad-member-container', $(this))
.filter(':odd')
.addClass('mc-ui-odd');
$('.mc-ui-squad-member-container', $(this))
.filter(':even')
.addClass('mc-ui-even');
}
);

if (widget.options.dynamic) {
$(element).accordion();
}
},
_setOption: function (key, value) {
// Use the _setOption method to respond to changes to options
$.Widget.prototype._setOption.apply(this, arguments)
},
destroy: function () {
// Use the destroy method to reverse everything your plugin has applied
$.Widget.prototype.destroy.call(this);
// do something
}
});
})(jQuery);
*/

// -- Template jQuery Widget --
/*
(function($) {
// The jQuery.aj namespace will automatically be created if it doesn't exist
$.widget("matchcentreui.widget", {
// These options will be used as defaults
options: { className : "" },
_create: function() {
// The _create method is where you set up the widget
},
_init: function() {
// The _init method is where you initialise or re-initialise the widget
},
// Keep various pieces of logic in separate methods
publicAction: function() {
// Methods without an underscore are "public"
},
_privateAction: function() {
// Methods with an underscore are "private"
}
_setOption: function(key, value) {
// Use the _setOption method to respond to changes to options
switch(key) {
case "option": // do something
break;
}
$.Widget.prototype._setOption.apply(this,arguments)
},
destroy: function() {
// Use the destroy method to reverse everything your plugin has applied
$.Widget.prototype.destroy.call(this);
// do something
}
});
})(jQuery);
*/

// -- JQuery Extensions for Form that post to Web Methods --
(function ($) {
    // The jQuery.matchcentreui namespace will automatically be created if it doesn't exist
    $.widget("matchcentreui.webmethodform", {
        // These options will be used as defaults
        options: {
            routerURL: '',
            fullClassName: '',
            submitMethodName: ''
        },
        _create: function () {
            // The _create method is where you set up the widget
            var widget = this;
            var element = this.element[0];
            this._setupSubmitClick(widget, element);
            this._setupCancelClick(widget, element);

            $('.mc-ui-button', element).button();
        },
        _init: function () {
            // The _init method is where you initialise or re-initialise the widget
            var widget = this;
            var element = this.element[0];
        },
        _setOption: function (key, value) {
            // Use the _setOption method to respond to changes to options
            $.Widget.prototype._setOption.apply(this, arguments)
        },
        _setupSubmitClick: function (widget, element) {
            $('.mc-ui-submit-button', element)
                .unbind('click')
                .click(
                    function (event) {
                        event.preventDefault();

                        if (typeof (WebForm_OnSubmit) == 'function' && WebForm_OnSubmit() == false) return;

                        var parameters = [];
                        $('form :input', element).each(
                            function () {
                                if (!$(this).attr('id').match(/^__/)) {
                                    if ($(this).is(':checkbox')) {
                                        if ($(this).is(':checked'))
                                            parameters.push($(this).attr('id') + ":'1'");
                                        else
                                            parameters.push($(this).attr('id') + ":'0'");
                                    } else {
                                        parameters.push($(this).attr('id') + ":'" + $(this).val() + "'");
                                    }
                                }
                            }
                        );
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: widget.options.routerURL,
                            data:
            "{" +
            "fullClassName:'" + widget.options.fullClassName + "'," +
            "methodName:'" + widget.options.submitMethodName + "'," +
            "parameters:{" + parameters.join(',') + "}}",
                            success: function (result) {
                                // Receive new contents.
                                var newElement = $(result.d);
                                // Insert new contents in DOM.
                                $(element).before($(newElement));
                                $(element).remove();
                            }
                        });
                        return false;
                    }
                );
        },
        _setupCancelClick: function (widget, element) {
            $('.mc-ui-cancel-button', element)
                .unbind('click')
                .click(
                    function (event) {
                        event.preventDefault();
                        $(document).trigger('close.facebox');
                    }
                );
        },
        destroy: function () {
            // Use the destroy method to reverse everything your plugin has applied
            $.Widget.prototype.destroy.call(this);
            // do something
        }
    });
})(jQuery);


/* -- Array filter type declaration --*/
if (!Array.prototype.filter) {
    Array.prototype.filter = function (fun /*, thisp*/) {
        var len = this.length;
        if (typeof fun != "function")
            throw new TypeError();

        var res = new Array();
        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in this) {
                var val = this[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, this))
                    res.push(val);
            }
        }

        return res;
    };
}

