﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bok.Master" AutoEventWireup="true" CodeBehind="Disclaimer.aspx.cs" Inherits="SuperSport.Rugby.BokSmart.Web.Disclaimer" %>
<%@ Register src="Controls/CustomGenericSiteContent.ascx" tagPrefix="uc" tagName="CustomGenericSiteContent" %>
<%@ Register src="~/MatchCentreUI/Web/SocialNetwork/GenericSocialNetworkMetaTags.ascx" tagname="SocialNetworkMetaTags" tagprefix="mc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>BOKSMART - Disclaimer</title>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-1.5.1.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-ui-1.8.14.custom.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.ThreeDots.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.dotdotdot-1.3.0.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/Flash/Script/swfobject.js") %>" type="text/javascript"></script>
    <mc:SocialNetworkMetaTags 
        ID="mcSocialNetworkMetaTags" 
        runat="server"
        OpenGraphMetaType="<%# SuperSport.MatchCentre.UI.Web.SocialNetwork.OpenGraphMetaType.Website %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="padding20"></div>
    <div class="spineline">
            <uc:CustomGenericSiteContent runat="server" ID="ucTopBannerAd"/>
        </div>
    <div class="block620">
        <div class="padding20"></div>
        <uc:CustomGenericSiteContent runat="server" ID="ucDisclaimer"/>
    </div>
    <div class="padding20"></div>
</asp:Content>
