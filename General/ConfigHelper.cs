﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SuperSport.Rugby.BokSmart.Web.General
{
    public static class ConfigHelper
    {
        const string M_IMAGEROOTURL = "http://images.supersport.com/";
        const string M_SARUGBYARTICLEURL = "http://www.sarugby.co.za/article.aspx?id={0}";

        public static string GetImageRootUrl()
        {
            string ReturnValue = M_IMAGEROOTURL;

            if (ConfigurationManager.AppSettings["ImageRootUrl"] != null)
            {
                ReturnValue = ConfigurationManager.AppSettings["ImageRootUrl"].Trim().AddTrailingSlash();
            }

            return ReturnValue;
        }

        public static string GetSaRugbyArticleUrl(int articleId)
        {
            string ReturnValue = M_SARUGBYARTICLEURL;

            if (ConfigurationManager.AppSettings["SARugbyArticleUrl"] != null)
            {
                ReturnValue = ConfigurationManager.AppSettings["SARugbyArticleUrl"].Trim();
            }

            return string.Format(ReturnValue, articleId.ToString());
        }

        public static string GetApplicationConfigSetting(string appSettingKeyName)
        {
            string ReturnValue = "";

            if (ConfigurationManager.AppSettings[appSettingKeyName] != null)
            {
                ReturnValue = ConfigurationManager.AppSettings[appSettingKeyName].Trim();
            }

            return ReturnValue;
        }
    }

    public static class StringExtensions
    {
        public static string AddTrailingSlash(this string value)
        {
            string Val = value.Trim();
            if (Val.Length > 0)
            {
                if (!Val.Substring(Val.Length - 1, 1).Equals("/"))
                {
                    return Val + "/";
                }
            }

            return Val;
        }
    }
}