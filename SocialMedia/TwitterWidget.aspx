﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TwitterWidget.aspx.cs" Inherits="SuperSport.Rugby.BokSmart.Web.SocialMedia.TwitterWidget" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <script charset="utf-8" src="http://widgets.twimg.com/j/2/widget.js" type="text/javascript"></script>
    <style type="text/css">
        .selectbox
        {
            border: 1px solid #D8DFEA;
            margin-bottom: 5px;
            margin-right: 5px;
            margin-top: 5px;
            padding: 5px;
            width: 280px;
            line-height: 18px;
        }
        
        .selectbox a
        {
            color: #004010;
            font-family: "lucida grande" ,tahoma,verdana,arial,sans-serif;
            font-size: 11px;
            text-align: left;
        }
        
        .body
        {
            font-family: "lucida grande" ,tahoma,verdana,arial,sans-serif;
            font-size: 11px;
            text-align: left;
        }
    </style>
</head>
<body>
<script type="text/javascript">
    function setUp() {
        BokTweet = new TWTR.Widget({
            version: 2,
            type: 'profile',
            rpp: 4,
            interval: 30000,
            title: 'Unite behind the Springboks',
            subject: 'Unite 2011',
            width: 280,
            height: 350,
            theme: {
                shell: {
                    background: '#004010',
                    color: '#e6dc20'
                },
                tweets: {
                    background: '#ffffff',
                    color: '#6b6b6b',
                    links: '#004010'
                }
            },
            features: {
                scrollbar: true,
                loop: true,
                live: true,
                behavior: 'default'
            }
        });
    }

    setUp();
    BokTweet.render().setUser('BokSmart').start();

    function setProfile(tProfile) {
        BokTweet.stop();
        BokTweet.destroy();
        BokTweet._isListWidget = false;
        BokTweet._isProfileWidget = true;
        BokTweet.render().setUser(tProfile).start();

    };
    function setList() {
        BokTweet.stop();
        BokTweet.destroy();
        BokTweet._isProfileWidget = true;
        BokTweet._isListWidget = false;
        BokTweet.subject = 'Unite 2011';
        BokTweet.title = 'Unite behind the Springboks';
        BokTweet.render().setUser('BokSmart').start();
    };
</script>
</body>
</html>
