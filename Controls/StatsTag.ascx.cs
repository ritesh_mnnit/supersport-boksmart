﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;

namespace SuperSport.Rugby.BokSmart.Web.Controls
{
    public partial class StatsTag : System.Web.UI.UserControl
    {
        public string StatsTagContentId
        {
            get
            {
                return Properties.Settings.Default.StatsTagContentId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}