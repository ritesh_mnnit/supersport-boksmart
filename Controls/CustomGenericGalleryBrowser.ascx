﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericGalleryBrowser.ascx.cs"
    Inherits="SuperSport.MatchCentre.UI.Web.Galleries.GenericGalleryBrowser" EnableViewState="false" %>
<%@ Import Namespace="SuperSport.MatchCentre.UI.Utils" %>
<asp:ListView ID="rptGallery" runat="server">
    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>
    <ItemTemplate>
        <asp:Image ID="imgPic" runat="server"  ToolTip='<%#Eval("Description")%>' ImageUrl='<%#SiteConfig.DefaultImageURLBase + Eval("LargeImage") %>' imageindex='<%#Container.DataItemIndex + 1 %>' data-thumb='<%#SiteConfig.DefaultImageURLBase + Eval("SmallImage") %>' />
    </ItemTemplate>
</asp:ListView>
