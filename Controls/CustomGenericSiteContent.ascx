﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericSiteContent.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Content.GenericSiteContent" EnableViewState="false" %>
<div  id="<%= ClientID %>" class=" <%=CssClass %>">
    <asp:Panel ID="pnlHeader" runat="server" CssClass="">
        <asp:Literal ID="LiteralSiteContentHeader" runat="server"></asp:Literal>
    </asp:Panel>
    <div class="">
        <asp:Literal ID="LiteralSiteContent" runat="server"></asp:Literal>
    </div>
</div>