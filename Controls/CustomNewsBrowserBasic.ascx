﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericNewsBrowser.ascx.cs" 
        Inherits="SuperSport.MatchCentre.UI.Web.News.GenericNewsBrowser" EnableViewState="false" %>

<div id="<%=ClientID%>">
<!-- Boksmart Feature BEGIN -->
    <asp:ListView ID="rptNews" runat="server" OnItemDataBound="rptNews_ItemDataBound">
        <LayoutTemplate>
            <div class="block300_left">
                <a href="#" title="test">
                    <img src="../Images/Boksmart_Placeholder_300x300.jpg" height="305" width="300" alt="Feature" border="0" />
                </a>
            </div>
            <div class="block320_right">
                <div id="itemPlaceholder" runat="server">
                </div>
            </div>
            <div class="featurefooter" id="featureContainer">
                <div class="padding10">
                </div>
                <div class="featuremore">
                    <a style="color: #FFFFFF; text-decoration: none;" href="../news/archive">...More
                        news</a></div>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="featureitem">
                <div class="padding3"></div>
                <div class="h2">
                    <a href="<%# GetArticleViewUrl(Convert.ToInt32(Eval("ID")), Eval("UrlFriendlyDate").ToString(), Eval("UrlFriendlyHeadline").ToString()) %>">
                        <%#Eval("Headline") %>
                    </a>
                </div>
                <div class="paddingbottom3" style="text-overflow: ellipsis">
                    <a href="<%# GetArticleViewUrl(Convert.ToInt32(Eval("ID")), Eval("UrlFriendlyDate").ToString(), Eval("UrlFriendlyHeadline").ToString()) %>">
                        <%# (Convert.ToString(Eval("Blurb")).Length > 100) ? Convert.ToString(Eval("Blurb")).Substring(0, 100) + "..." : Eval("Blurb")%>
                    </a>
                </div>
                <div id="hdnImageUrl" style="display:none;">
                    <a href="<%# GetArticleViewUrl(Convert.ToInt32(Eval("ID")), Eval("UrlFriendlyDate").ToString(), Eval("UrlFriendlyHeadline").ToString()) %>">
                        <img src="<%=SuperSport.Rugby.BokSmart.Web.General.ConfigHelper.GetImageRootUrl()%><%#Eval("ExtraImageName")%>" height="305" width="300" alt="Feature" border="0"/>
                    </a>
                </div>
            </div>
        </ItemTemplate>
    </asp:ListView>
<!-- Boksmart Feature END -->
</div>

