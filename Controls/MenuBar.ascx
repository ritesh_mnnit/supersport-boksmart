﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuBar.ascx.cs" Inherits="SuperSport.Rugby.BokSmart.Web.Controls.MenuBar" %>
<asp:SqlDataSource 
        ID="NavMenuSqlDataSource" 
        runat="server" 
        SelectCommand="SELECT * FROM supersportzone.dbo.zonesitecontent WHERE site = 16 AND id=1094"
        ConnectionString="<%$ ConnectionStrings:connRead %>">
    </asp:SqlDataSource>

    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="NavMenuSqlDataSource">
        <ItemTemplate>
            <%#DataBinder.Eval(Container.DataItem, "Content") %>
        </ItemTemplate>
    </asp:Repeater>