﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericArticleView.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.News.GenericArticleView" EnableViewState="false" %>
<div  id="<%= ClientID %>" class="mc-ui-news-article-widget ui-widget ui-widget-content <%=CssClass %>">
    <span class="mc-ui-news-article-category">
        <asp:Literal ID="LiteralArticleCategory" runat="server"></asp:Literal>
    </span>
    <br class="mc-ui-news-article-category-break" />

    <span class="mc-ui-news-article-pic-container">
        <asp:Image ID="imgArticlePic" runat="server" CssClass="mc-ui-news-article-pic" />
        <br class="mc-ui-news-article-pic-break" />
        <span class="mc-ui-news-article-pic-caption">
            <asp:Literal ID="LiteralArticlePicCaption" runat="server"></asp:Literal>
        </span>
    </span>
    <%--<br class="mc-ui-news-article-pic-container-break" />
    <br class="mc-ui-news-article-headline-break" />--%>
    <![if !IE]>
        <br class="mc-ui-news-article-headline-break" />
        <br class="mc-ui-news-article-headline-break" />
    <![endif]>
    <table border="0">
        <tr>
            <td>
            <span class="mc-ui-news-article-headline">
        <asp:Literal ID="LiteralArticleHeadline" runat="server"></asp:Literal>
    </span>
            </td>
        </tr>
        <tr>
            <td>
            <span class="mc-ui-news-article-timestamp">
        <asp:Literal ID="LiteralArticleTimestamp" runat="server"></asp:Literal>
    </span>
            </td>
        </tr>
    </table>
<%--    <br class="mc-ui-news-article-headline-break" />

    --%>
    <%--<br class="mc-ui-news-article-timestamp-break" />--%>

    <span class="mc-ui-news-article-author">
        <asp:Literal ID="LiteralArticleAuthor" runat="server"></asp:Literal>
    </span>
    <br class="mc-ui-news-article-author-break" />

    <asp:Panel ID="pnlTopSharing" runat="server" CssClass="mc-ui-social-network-like">
        <fb:like href="<%= HttpContext.Current.Request.Url.AbsoluteUri %>" send="false" layout="<%= FacebookLikeLayout %>" show_faces="false" width="<%= FacebookLikeWidth %>" action="like" border_color="" stream="false" header="false"></fb:like>
    </asp:Panel>
    <br class="mc-ui-social-network-like-break" />

    <div class="mc-ui-news-article-body">
        <asp:Literal ID="LiteralArticleBody" runat="server"></asp:Literal>
    </div>
    <br class="mc-ui-news-article-body-break" />

    <span class="mc-ui-news-article-extra-pic-container">
        <asp:Image ID="imgArticleExtraPic" runat="server" CssClass="mc-ui-news-article-extra-pic" />
        <br class="mc-ui-news-article-extra-pic-break" />
        <span class="mc-ui-news-article-extra-pic-caption">
            <asp:Literal ID="LiteralArticleExtraPicCaption" runat="server"></asp:Literal>
        </span>
    </span>
    <br class="mc-ui-news-article-extra-pic-container-break" />
    <asp:Panel ID="pnlBotSharing" runat="server" CssClass="mc-ui-news-article-sharing">
        <span class="mc-ui-local-sharing">
            <asp:HyperLink ID="lnkPrint" runat="server" CssClass="mc-ui-print-button" Target="_blank" ToolTip="Print">
                <img src="<%= (!string.IsNullOrEmpty(PrintImage) ? PrintImage : "http://www.supersport.com/images/sm_printer.png" ) %>" alt="Print" />
            </asp:HyperLink>
            <asp:HyperLink ID="lnkEMail" runat="server" CssClass="mc-ui-email-button" Target="_blank" ToolTip="E-Mail" Visible="false">
                <img src="<%= (!string.IsNullOrEmpty(EMailImage) ? EMailImage : "http://www.supersport.com/images/sm_mail.png" ) %>" alt="E-Mail" />
            </asp:HyperLink>
        </span>
        <span class="mc-ui-social-sharing">
            <asp:HyperLink ID="lnkTwitter" runat="server" Target="_blank">
                <img src="http://www.supersport.com/images/sm_twitter.png" alt="Twitter" title="Twitter" />
            </asp:HyperLink>
            <asp:HyperLink ID="lnkFacebook" runat="server" Target="_blank">
                <img src="http://www.supersport.com/images/sm_fb.png" alt="Facebook" title="Facebook" />
            </asp:HyperLink>
        </span>
    </asp:Panel>
    <br class="mc-ui-news-article-sharing-break" />

    <div class="mc-ui-social-network-comment">
        <fb:comments href="<%= HttpContext.Current.Request.Url.AbsoluteUri %>"  num_posts="10" width="<%= FacebookCommentWidth %>"></fb:comments>    
    </div>
    <br class="mc-ui-social-network-comment-break" />
    
    <br class="mc-ui-news-article-break" />
</div>
