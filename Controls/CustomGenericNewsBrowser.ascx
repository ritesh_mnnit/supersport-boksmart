﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericNewsBrowser.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.News.GenericNewsBrowser" EnableViewState="false" %>
<asp:ListView ID="rptNews" runat="server" OnItemDataBound="rptNews_ItemDataBound">
    <LayoutTemplate>
        <div class="padding10"></div>
            <div runat="server" id="itemPlaceHolder"></div>
        <div class="padding10"></div>
    </LayoutTemplate>
    <ItemTemplate>
        <div id="itemPlaceHolder">
            <a href="<%#SuperSport.Rugby.BokSmart.Web.General.ConfigHelper.GetSaRugbyArticleUrl(Convert.ToInt32(Eval("ID")))%>">
                <%# Eval("Headline")%>
            </a>
            <div class="dotted"></div>
            <div class="padding5"></div>
        </div>
    </ItemTemplate>
    <ItemSeparatorTemplate>
    </ItemSeparatorTemplate>
    <EmptyDataTemplate>
    </EmptyDataTemplate>
</asp:ListView>
