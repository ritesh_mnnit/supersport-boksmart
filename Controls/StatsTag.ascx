﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatsTag.ascx.cs" Inherits="SuperSport.Rugby.BokSmart.Web.Controls.StatsTag" %>
<asp:SqlDataSource 
    ID="StatsSqlDataSource" 
    runat="server" 
    SelectCommand="select * from supersportzone.dbo.zonesitecontent where id = <%=StatsTagContentId%>"
    ConnectionString="<%$ ConnectionStrings:connRead %>">
</asp:SqlDataSource>
<asp:Repeater ID="Repeater1" runat="server" DataSourceID="StatsSqlDataSource">
    <ItemTemplate>
        <%#DataBinder.Eval(Container.DataItem, "Content") %>
    </ItemTemplate>
</asp:Repeater>