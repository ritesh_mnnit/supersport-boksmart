﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericNewsBrowser.ascx.cs" 
        Inherits="SuperSport.MatchCentre.UI.Web.News.GenericNewsBrowser" EnableViewState="false" %>

<div id="<%=ClientID%>">
<!-- Boksmart Feature BEGIN -->
    <asp:ListView ID="rptNews" runat="server" OnItemDataBound="rptNews_ItemDataBound">
        <LayoutTemplate>
            <div id="MainDiv">
                <div id="itemPlaceholder" runat="server"></div>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="archivenewsitem">
                <a href="<%# GetArticleViewUrl(Convert.ToInt32(Eval("ID")), Eval("UrlFriendlyDate").ToString(), Eval("UrlFriendlyHeadline").ToString()) %>">
                    <%# (Convert.ToString(Eval("Headline")).Length < 100) ? Eval("Headline") : Convert.ToString(Eval("Headline")).Substring(0, 34) + "..." %>
                </a>
                &nbsp;-&nbsp;
                <span class="newsarticledate">
                    <%# Convert.ToDateTime(Eval("DateCreated")).ToString("MMM dd, yyyy")  %>
                </span>
            </div>
        </ItemTemplate>
    </asp:ListView>
<!-- Boksmart Feature END -->
</div>

