﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericGalleriesBrowser.ascx.cs"
    Inherits="SuperSport.MatchCentre.UI.Web.Galleries.GenericGalleriesBrowser" EnableViewState="false" %>
<asp:ListView ID="rptGalleries" runat="server" OnItemDataBound="rptGalleries_ItemDataBound">
    <ItemTemplate>
        <td>
            <div class="Gallery">
                <asp:HyperLink runat="server" NavigateUrl='<%#Eval("Id","/Gallery/{0}") %>'>
                    <div>
                        <%#Eval("DateCreated","{0:dd MMMM yyyy}")%></div>
                    <div>
                        <img src='<%#Eval("SmallImage","http://images.supersport.com/{0}") %>' alt="" />
                    </div>
                    <div class="Headline">
                        <%#Eval("Headline")%>
                    </div>
                </asp:HyperLink>
            </div>
        </td>
    </ItemTemplate>
    <LayoutTemplate>
        <br style="clear: both" />
        <div class="PhotoGalleries">
            <table>
                <tr runat="server" id="groupPlaceholder" />
            </table>
        </div>
        <br style="clear: both" />
    </LayoutTemplate>
    <GroupTemplate>
        <tr>
            <td runat="server" id="itemPlaceholder" />
        </tr>
    </GroupTemplate>
    <EmptyDataTemplate>
        <span class="mc-ui-empty-message">There are currently no galleries available. </span>
    </EmptyDataTemplate>
</asp:ListView>
<script type="text/javascript">
    $(function () {
        $(".PhotoGalleries .Gallery a").colorbox({
            iframe: true,
            innerWidth: 650,
            innerHeight: 550
        });
    });

    function showProgress(progress) {
        $("#cboxTitle").html(progress);
    }
</script>
