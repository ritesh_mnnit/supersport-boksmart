﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="SuperSport.Rugby.BokSmart.Web.Media.Photo.Gallery" %>
<%@ Register Src="~/Controls/CustomGenericGalleryBrowser.ascx" TagName="GalleryBrowser"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        div.slider-wrapper {            
	        margin: 20px auto;
        }
        .nivo-thumbs-enabled a img {
            width: 36px !important;
        }
    </style>
    <link href="/Styles/nivoslider/nivo-slider.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/nivoslider/themes/default/default.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/Scripts/jquery.nivo.slider.pack.js") %>"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $('#slider').nivoSlider({
                animSpeed: 200,
                effect: 'fade',
                manualAdvance: true,
                controlNavThumbs: true,
                controlNavThumbsFromRel: true,
                afterChange: function () {
                    resizeColorbox();
                    showImageCounting();
                },
                afterLoad: function () {
                    resizeColorbox();
                    showImageCounting();
                }
            });
        });

        function resizeColorbox() {
            var sliderHeight = $(".slider-wrapper").height();
            parent.$.colorbox.resize({ width: 600, height: sliderHeight + 120 });
        }

        function showImageCounting() {
            var totalImages = $("#slider").children("img").not(".nivo-main-image").length;
            var mainImageSrc = $("#slider .nivo-main-image").attr("src");
            var image = $("#slider").children("img[src='" + mainImageSrc + "']").not(".nivo-main-image");
            parent.showProgress(image.attr("imageindex") + " of " + totalImages);             
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="slider-wrapper theme-default">
            <div id="slider" class="nivoSlider">
                <uc:GalleryBrowser ID="ucGalleryBrowser" runat="server" CssClass="gallery-page" />
            </div>
        </div>
    </form>
</body>
</html>
