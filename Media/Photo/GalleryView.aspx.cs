﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuperSport.Rugby.BokSmart.Web.Media.Photo
{
    public partial class GalleryView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ucGalleryBrowser.DataBind();
        }
    }
}