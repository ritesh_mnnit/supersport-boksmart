﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GalleryView.aspx.cs" MasterPageFile="~/Bok.Master"
    Inherits="SuperSport.Rugby.BokSmart.Web.Media.Photo.GalleryView" %>
<%@ Register Src="~/Controls/CustomGenericGalleryBrowser.ascx" TagName="GalleryBrowser"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-1.5.1.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-ui-1.8.14.custom.js") %>"
        type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.ThreeDots.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.dotdotdot-1.3.0.js") %>"
        type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/galleria/galleria-1.2.5.min.js") %>"
        type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/matchcentreui.js") %>" type="text/javascript"></script>
    <script src="../../Scripts/NFLightBox.js" type="text/javascript"></script>
    <link href="../../Styles/nf.lightbox.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("a.GalleryImage").lightBox();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="padding20">
    </div>
    <div class="block620">
        <span class="mc-ui-content-header">Photo Gallery</span>
        <div class="padding5">
        </div>
        <uc:GalleryBrowser ID="ucGalleryBrowser" runat="server" CssClass="gallery-page" />
        <p>
        </p>
        <a href="../galleries" style="color: mediumseagreen; font-style: italic">back to galleries</a>
    </div>
    <div class="padding20">
    </div>
</asp:Content>
