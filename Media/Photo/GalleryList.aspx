﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bok.Master" AutoEventWireup="true"
    CodeBehind="GalleryList.aspx.cs" Inherits="SuperSport.Rugby.BokSmart.Web.Media.Photo.GalleryList" %>

<%@ Register Src="~/Controls/CustomGenericGalleriesBrowser.ascx" TagName="GalleriesBrowser"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-1.5.1.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-ui-1.8.14.custom.js") %>"
        type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.ThreeDots.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.dotdotdot-1.3.0.js") %>"
        type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/facebox/facebox.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/galleria/galleria-1.2.5.min.js") %>"
        type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/matchcentreui.js") %>" type="text/javascript"></script>
    <link href="<%= ResolveUrl("~/Styles/colorbox/colorbox.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%= ResolveUrl("~/Scripts/jquery.colorbox-min.js") %>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block620">
        <div class="padding10">
        </div>
        <div class="GalleryTitle">Photo Galleries</div>
        <uc:GalleriesBrowser ID="ucGalleriesBrowser" runat="server" CssClass="" Count="20"
            GroupCount="3" IsDynamicPaging="true" />
    </div>

</asp:Content>
