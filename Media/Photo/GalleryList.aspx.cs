﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;

namespace SuperSport.Rugby.BokSmart.Web.Media.Photo
{
    public partial class GalleryList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ucGalleriesBrowser.SiteID = SiteConfig.SiteID;
            ucGalleriesBrowser.DataBind();
        }
    }
}