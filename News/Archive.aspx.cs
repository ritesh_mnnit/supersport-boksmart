﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.Rugby.BokSmart.Web.General;

namespace SuperSport.Rugby.BokSmart.Web.News
{
    public partial class Archive : System.Web.UI.Page
    {
        private int m_CurrentPageNumber = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            SetCurrentPageNumber();

            if (!IsPostBack)
            {
                BindNewsControl();
            }
            
            lblCount.Text = ucCustomNewsArchiveBrowser.Count.ToString();
        }

        /// <summary>
        /// Configures the page details and binds the control
        /// </summary>
        private void BindNewsControl()
        {
            SetPagingInfo();
            ucCustomNewsArchiveBrowser.SiteID = int.Parse(ConfigHelper.GetApplicationConfigSetting("NewsSiteId"));
            ucCustomNewsArchiveBrowser.DataBind();
        }

        /// <summary>
        /// Sets up the current page number for the archive
        /// </summary>
        /// <param name="incrementValue"></param>
        private void SetCurrentPageNumber(int incrementValue = 0)
        {
            int PageNumber = 1;
            if (RouteData != null)
            {
                if (RouteData.Values["page"] != null)
                {
                    int.TryParse(RouteData.Values["page"].ToString(), out PageNumber);
                    
                }
            }

            m_CurrentPageNumber = PageNumber > 0 ? PageNumber + incrementValue : m_CurrentPageNumber + incrementValue;
            if (m_CurrentPageNumber < 1)
            {
                m_CurrentPageNumber = 1;
            }
            else if (m_CurrentPageNumber > ucCustomNewsArchiveBrowser.Count)
            {
                m_CurrentPageNumber = ucCustomNewsArchiveBrowser.Count;
            }
        }

        /// <summary>
        /// Event handler for specifying the page to display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOk_OnClick(object sender, EventArgs e)
        {
            int NewPageNumber = 1;
            if (int.TryParse(txtPageNumber.Text.Trim(), out NewPageNumber))
            {
                m_CurrentPageNumber = NewPageNumber;
                m_CurrentPageNumber = (m_CurrentPageNumber > ucCustomNewsArchiveBrowser.Count ? 20 : m_CurrentPageNumber);
            }
            else
            {
                m_CurrentPageNumber = 1;
            }
            RedirectToNewPage();
        }

        /// <summary>
        /// Event handler for moving to the previous page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrev_OnClick(object sender, EventArgs e)
        {
            SetCurrentPageNumber(-1);
            RedirectToNewPage();
        }

        /// <summary>
        /// Event handler for moving to the next page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNext_OnClick(object sender, EventArgs e)
        {
            SetCurrentPageNumber(1);
            RedirectToNewPage();
        }

        /// <summary>
        /// Handles the redirection to the correct page, including the page details in the querystring
        /// </summary>
        private void RedirectToNewPage()
        {
            Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) + string.Format("/News/Archive/{0}", m_CurrentPageNumber));
        }

        /// <summary>
        /// Sets the pagibng info on the matchcentre control
        /// </summary>
        private void SetPagingInfo()
        {
            ucCustomNewsArchiveBrowser.Index = m_CurrentPageNumber - 1;
            txtPageNumber.Text = m_CurrentPageNumber.ToString();
        }
    }
}