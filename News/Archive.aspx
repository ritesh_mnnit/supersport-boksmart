﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bok.Master" AutoEventWireup="true"
    CodeBehind="Archive.aspx.cs" Inherits="SuperSport.Rugby.BokSmart.Web.News.Archive" %>

<%@ Register Src="~/Controls/CustomNewsArchiveBrowserBasic.ascx" TagName="CustomNewsArchiveBrowserBasic"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>BOKSMART - News Archive</title>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-1.5.1.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-ui-1.8.14.custom.js") %>"
        type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.ThreeDots.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.dotdotdot-1.3.0.js") %>"
        type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="padding20"></div>
    <div class="block620">
        <span class="newsheadline">News Article Archive </span>
        <hr />
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="40" class="newspager">
                    &nbsp;PAGE
                </td>
                <td width="40">
                    <asp:TextBox runat="server" ID="txtPageNumber" MaxLength="4" CssClass="pageNumber"
                        TextMode="SingleLine"></asp:TextBox>
                    <%--<input name="ctl00$cp_primary$f_page" type="text" value="1" maxlength="4" id="ctl00_cp_primary_f_page"
                    class="edit_page" />--%>
                </td>
                <td width="40" class="newspager">
                    of
                    <asp:Label ID="lblCount" runat="server"></asp:Label>
                </td>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" valign="middle" style="padding: 0 2px 0 0;">
                                <asp:Button runat="server" ID="btnOk" Text="OK" CssClass="btn_grey" OnClick="btnOk_OnClick" />
                                <%--<input type="submit" name="ctl00$cp_primary$btn_ok" value="OK" id="ctl00_cp_primary_btn_ok"
                                class="btn_grey" />--%>
                            </td>
                            <td align="center" valign="middle" style="padding: 0 2px 0 2px;">
                                <asp:Button runat="server" ID="btnPrev" Text="PREVIOUS" CssClass="btn_grey" OnClick="btnPrev_OnClick" />
                                <%--<input type="submit" name="ctl00$cp_primary$btn_prev" value="PREVIOUS" id="ctl00_cp_primary_btn_prev"
                                class="btn_grey" />--%>
                            </td>
                            <td align="center" valign="middle" style="padding: 0 0 0 2px;">
                                <asp:Button runat="server" ID="btnNext" Text="NEXT" CssClass="btn_grey" OnClick="btnNext_OnClick" />
                                <%--<input type="submit" name="ctl00$cp_primary$btn_next" value="NEXT" id="ctl00_cp_primary_btn_next"
                                class="btn_grey" />--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div style="height: 20px; clear: both;">
        </div>
        <uc:CustomNewsArchiveBrowserBasic ID="ucCustomNewsArchiveBrowser" runat="server"
            Count="20" />
    </div>
</asp:Content>
