﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Web.SocialNetwork;

namespace SuperSport.Rugby.BokSmart.Web.News
{
    public partial class Article : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckAndSetArchivePageLink();
            mcArticleMetaTags.OpenGraphMetaType = OpenGraphMetaType.Article;
            mcArticleMetaTags.FacebookMetaAppID = "119034421542340";
            mcArticleMetaTags.DataBind();

            mcArticleView.DataBind();
        }

        /// <summary>
        /// Checks for an archive page number in the referring url
        /// </summary>
        private void CheckAndSetArchivePageLink()
        {
            int ArchivePageNumber = GetArchivePageNumber();
            if (ArchivePageNumber > 0)
            {
                lnkArchive.HRef = Request.Url.GetLeftPart(UriPartial.Authority) + string.Format("/news/archive/{0}", ArchivePageNumber.ToString());
                lnkArchive.Visible = true;
            }
        }

        /// <summary>
        /// Gets the archive page number from the querystring
        /// </summary>
        /// <returns></returns>
        private int GetArchivePageNumber()
        {
            int PageNumber = 0;
            if (RouteData.Values["page"] != null)
            {
                int.TryParse(RouteData.Values["page"].ToString(), out PageNumber);
            }
            return PageNumber;
        }
    }
}