﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bok.Master" AutoEventWireup="true"
    CodeBehind="Article.aspx.cs" Inherits="SuperSport.Rugby.BokSmart.Web.News.Article" %>

<%@ Register Src="~/MatchCentreUI/Web/News/GenericArticleMetaTags.ascx" TagName="ArticleMetaTags"
    TagPrefix="mc" %>
<%@ Register Src="~/Controls/CustomGenericArticleView.ascx" TagName="ArticleView"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>BOKSMART - News Article</title>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-1.5.1.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-ui-1.8.14.custom.js") %>"
        type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.ThreeDots.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.dotdotdot-1.3.0.js") %>"
        type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/facebox/facebox.js") %>" type="text/javascript"></script>
    <link href="<%= ResolveUrl("~/MatchCentreUI/facebox/facebox.css") %>" rel="stylesheet"
        type="text/css" />
    <mc:ArticleMetaTags ID="mcArticleMetaTags" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="padding20">
    </div>
    <div class="block620">
        <div class="newsheadline" >
            <div style="float: left;">News Articles </div>
            <div class="newsarchivesmall">
                <a href="/news/archive" id="lnkArchive" visible="true" runat="server"><< TO NEWS ARCHIVE</a>
            </div>
        </div>
        <span style="width: 100%; float: left;">
            <hr />
        </span>
        <uc:ArticleView ID="mcArticleView" runat="server" CssClass="articleItem" />
    </div>
</asp:Content>
