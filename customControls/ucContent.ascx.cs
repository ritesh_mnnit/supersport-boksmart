﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.CMS.Models;
using System.ComponentModel;

namespace SuperSport.Rugby.BokSmart.Web.customControls
{
    public partial class ucContent : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets or sets the ID for the content item to be retrieved.
        /// </summary>
        public int ContentID { get; set; }

        /// <summary>
        /// Gets or sets the SEO friendly url headline for the content item to be retrieved.
        /// </summary>
        public string ContentHeadline { get; set; }

        /// <summary>
        /// Gets or sets the SEO friendly url content category for the content item to be retrieved.
        /// </summary>
        public string ContentCategory { get; set; }

        /// <summary>
        /// Gets or sets the SEO friendly url content category for the content item to be retrieved 
        /// if no content item could be retrieved for the first choice category.
        /// </summary>
        public string FallbackContentCategory { get; set; }

        /// <summary>
        /// Gets or sets the SiteID for which results should be retrieved.
        /// </summary>
        public int SiteID { get; set; }

        /// <summary>
        /// Gets or sets the Cascading Style Sheet (CSS) class.
        /// </summary>
        public string CssClass { get; set; }

        /// <summary>
        /// Gets or sets whether the content header should be displayed.
        /// </summary>
        [DefaultValue(true)]
        public bool DisplayHeader { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Were any overides for the article searching criteria specified
            // in the code behind of the containing page?
            if (string.IsNullOrEmpty(ContentCategory) &&
                (null != Page.RouteData.Values["category"]))
            {
                ContentCategory = Page.RouteData.Values["category"].ToString();
            }

            if (string.IsNullOrEmpty(ContentHeadline) &&
                (null != Page.RouteData.Values["contentheadline"]))
            {
                ContentHeadline = Page.RouteData.Values["contentheadline"].ToString();
            }

            if (0 == ContentID)
            {
                int contentID = 0;
                bool result = int.TryParse(ContentHeadline, out contentID);
                ContentID = contentID;
            }

            SuperSport.CMS.Models.Content content = null;

            if (0 != ContentID)
            {
                content = SuperSport.CMS.Models.Content.GetContent(ContentID);
            }
            else if (!string.IsNullOrEmpty(ContentHeadline))
            {
                if (0 != SiteID)
                {
                    content = SuperSport.CMS.Models.Content.GetContent(SiteID, ContentCategory, ContentHeadline);
                    if (null == content)
                        content = SuperSport.CMS.Models.Content.GetContent(SiteID, FallbackContentCategory, ContentHeadline);
                }
                else
                {
                    throw new Exception(
                        "Failed to retrieve Content from CMS Centre.",
                        new NullReferenceException("A SiteID must be specified."));
                }
            }
            else
            {
                throw new Exception(
                    "Failed to retrieve Content.",
                    new NullReferenceException("A ContentID or ContentHeadline must be specified."));
            }

            if (null != content)
            {
                if (!string.IsNullOrEmpty(content.WebContent))
                {
                    ;
                    LiteralContent.Text = content.WebContent.Replace("PATH_TO_BOKSMART_PDF_DOC/", System.Configuration.ConfigurationManager.AppSettings["PdfFullUri"]);
                }
                else if (!string.IsNullOrEmpty(content.MobiContent))
                {
                    LiteralContent.Text = content.MobiContent;
                }
                else
                {
                    LiteralContent.Visible = false;
                }

                if (!string.IsNullOrEmpty(content.Headline) && DisplayHeader)
                {
                    pnlHeader.Visible = true;
                    LiteralHeader.Visible = true;
                    LiteralHeader.Text = content.Headline;
                }
                else
                {
                    pnlHeader.Visible = false;
                    LiteralHeader.Visible = false;
                }
            }
            else
            {
                LiteralContent.Visible = false;
                pnlHeader.Visible = false;
                LiteralHeader.Visible = false;
            }
        }
    }
}