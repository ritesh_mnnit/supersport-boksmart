﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucContent.ascx.cs" Inherits="SuperSport.Rugby.BokSmart.Web.customControls.ucContent" %>
<div  class="mc-ui-content-widget ui-widget ui-widget-content ">
    <asp:Panel ID="pnlHeader" runat="server" CssClass="mc-ui-content-header">
        <asp:Literal ID="LiteralHeader" runat="server"></asp:Literal>
    </asp:Panel>
    <div class="mc-ui-content-body">
        <asp:Literal ID="LiteralContent" runat="server"></asp:Literal>
    </div>
</div>