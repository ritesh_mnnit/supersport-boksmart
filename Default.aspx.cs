﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Web.News;
using SuperSport.MatchCentre.UI.Web.Videos;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.Rugby.BokSmart.Web.General;

namespace BokSmartWeb
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NewsBrowser1.SiteID = int.Parse(ConfigHelper.GetApplicationConfigSetting("NewsSiteId"));
            NewsBrowser1.Category = "boksmart";
            NewsBrowser1.NewsHeadlineRetrievalType = NewsHeadlineRetrievalType.Archived;
            NewsBrowser1.DataBind();

            mcSocialNetworkMetaTags.DataBind();

            ucFacebookWidget.SiteID = SiteConfig.SiteID;
            ucFacebookWidget.ContentType = "FacebookWidget";
            ucFacebookWidget.DataBind();

            ucTopBannerAd.SiteID = SiteConfig.SiteID;
            ucTopBannerAd.ContentType = "TopBannerAd";
            ucTopBannerAd.DataBind();

            ucYoutubeChannel.SiteID = SiteConfig.SiteID;
            ucYoutubeChannel.ContentType = "YoutubeChannel";
            ucYoutubeChannel.DataBind();
        }

        /// <summary>
        /// Handles the redirection to the content page for old links that reference the default page
        /// for loading content
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //verify that if a content id has been specified, we redirect to the content page
            var ContentId = Request.QueryString["contentId"];
            if (ContentId != null)
            {
                Response.Redirect(string.Format("Content/{0}", ContentId));
                Response.End();
            }
        }
    }
}
