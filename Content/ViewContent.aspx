﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bok.Master" AutoEventWireup="true"
    CodeBehind="ViewContent.aspx.cs" Inherits="SuperSport.Rugby.BokSmart.Web.Content.ViewContent" %>

<%@ Register Src="~/customControls/ucContent.ascx" TagName="GenericContent"
    TagPrefix="mc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>BOKSMART - View Content</title>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-1.5.1.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-ui-1.8.14.custom.js") %>"
        type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.ThreeDots.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.dotdotdot-1.3.0.js") %>"
        type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="padding20">
    </div>
    <div class="block620">
        <mc:GenericContent runat="server" ID="mcGenericContent" DisplayHeader="True" />
    </div>
    <div class="padding20">
    </div>
</asp:Content>
