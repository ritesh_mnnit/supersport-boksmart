﻿using System;
using System.Linq;
using System.Web.UI;
using SuperSport.MatchCentre.UI.Utils;

namespace SuperSport.Rugby.BokSmart.Web.Content
{
    public partial class ViewContent : Page
    {
        private string GetContentIdFromQueryString()
        {
            var returnValue = "";
            var queryString = Request.Url.Query;

            if (queryString.Trim() != "")
            {
                var contentIdStart = queryString.IndexOf("contentId=", StringComparison.Ordinal) + 10;
                var nextQueryParam = queryString.IndexOf("&", contentIdStart, StringComparison.Ordinal);
                var endOfString = queryString.Length - contentIdStart;

                returnValue = queryString.Substring(queryString.IndexOf("contentId=", StringComparison.Ordinal) + 10,
                    (nextQueryParam == -1 ? endOfString : nextQueryParam - contentIdStart));
            }

            return returnValue;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (RouteData.Values.Count > 0)
            {
                //verify which route has been supplied
                var contentTitle = RouteData.Values.SingleOrDefault(r => r.Key == "title").Value;
                var contentId = RouteData.Values.SingleOrDefault(r => r.Key == "id").Value;
                var documentName = RouteData.Values.SingleOrDefault(r => r.Key == "doc").Value;

                if (contentTitle != null)
                {
                    if (contentTitle.ToString().ToLower().Equals("boksmart.aspx"))
                    {
                        //get the content id from the querystring
                        contentId = GetContentIdFromQueryString();
                        mcGenericContent.ContentID = int.Parse(contentId.ToString());
                        mcGenericContent.SiteID = SiteConfig.SiteID;
                        mcGenericContent.DataBind();
                    }
                    else
                    {
                        //use the content title supplied
                        mcGenericContent.ContentCategory = "boksmart";
                        mcGenericContent.SiteID = SiteConfig.SiteID;
                        mcGenericContent.ContentHeadline = contentTitle.ToString().Trim();
                        mcGenericContent.DataBind();
                    }
                }
                else if (contentId != null && documentName == null)
                {
                    mcGenericContent.ContentID = int.Parse(contentId.ToString());
                    mcGenericContent.DataBind();
                }
                else if (documentName != null)
                {
                    //redirect to correct location to veiw pdf
                    Response.Redirect(General.ConfigHelper.GetApplicationConfigSetting("PdfFullUri") + documentName);
                    Response.End();
                }
            }


        }
    }
}