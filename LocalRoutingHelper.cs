﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.Routing;
using SuperSport.MatchCentre.UI.Utils.Web;

namespace SuperSport.MatchCentre.UI.Utils
{
    public class LocalRoutingHelper : RoutingHelper
    {

        #region -- Properties --

        /// <summary>
        /// Get the route name for the news archive.
        /// </summary>
        public static string NewsArchiveRoute
        {
            get
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NewsArchiveRoute"]))
                {
                    return ConfigurationManager.AppSettings["NewsArchiveRoute"];
                }
                else
                {
                    return "NewsArchiveRoute";
                }
            }
        }

        /// <summary>
        /// Get the route name for the news archive.
        /// </summary>
        public static string PollArchiveRoute
        {
            get
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["PollArchiveRoute"]))
                {
                    return ConfigurationManager.AppSettings["PollArchiveRoute"];
                }
                else
                {
                    return "PollArchiveRoute";
                }
            }
        }

        /// <summary>
        /// Get the route name for the search page.
        /// </summary>
        public static string SearchRoute
        {
            get
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SearchRoute"]))
                {
                    return ConfigurationManager.AppSettings["SearchRoute"];
                }
                else
                {
                    return "SearchRoute";
                }
            }
        }

        #endregion

        /// <summary>
        /// Register the necessary routes for the news archive.
        /// </summary>
        public static void BuildNewsArchiveRouting(string urlBase, string newsArchivePage)
        {
            urlBase = urlBase.EndsWith("/") ? urlBase : urlBase + "/";
            RouteTable.Routes.MapPageRoute(NewsArchiveRoute, urlBase + "{page}", newsArchivePage);
        }

        /// <summary>
        /// Register the necessary routes for the news archive.
        /// </summary>
        public static void BuildPollArchiveRouting(string urlBase, string pollArchivePage)
        {
            RouteValueDictionary defaults = new RouteValueDictionary { { "page", 1 } };
            urlBase = urlBase.EndsWith("/") ? urlBase : urlBase + "/";
            RouteTable.Routes.MapPageRoute(PollArchiveRoute, urlBase + "{page}", pollArchivePage, false, defaults);
        }

        /// <summary>
        /// Register the necessary routes for the news archive.
        /// </summary>
        public static void BuildSearchRouting(string urlBase, string searchPage)
        {
            urlBase = urlBase.EndsWith("/") ? urlBase : urlBase + "/";
            RouteTable.Routes.MapPageRoute(SearchRoute, urlBase, searchPage);
        }

        /// <summary>
        /// Register the necessary routes for the content page.
        /// </summary>
        public static void BuildContentRouting(string urlBase, string searchPage)
        {
            urlBase = urlBase.EndsWith("/") ? urlBase : urlBase + "/";
            RouteTable.Routes.MapPageRoute("CustomContentRoute", urlBase, searchPage);
        }

        /// <summary>
        /// Register the necessary routes for the pdf documents.
        /// </summary>
        public static void BuildPdfContentRouting(string urlBase, string searchPage)
        {
            urlBase = urlBase.EndsWith("/") ? urlBase : urlBase + "/";
            RouteTable.Routes.MapPageRoute("CustomPdfContentRoute", urlBase, searchPage);
        }
    }
}
