﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoogleSearchBox.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.SearchEngine.GoogleSearchBox" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-search-box-widget ui-widget ui-widget-content <%=CssClass %>">
    <form method="<%= FormMethod %>" action="<%= FormAction %>">
    <div class="mc-ui-search-input-container">
        <input id="q" name="q" type="text" class="mc-ui-search-input" />
    </div>
    <div class="mc-ui-search-button-container">
        <% if (string.Equals(ButtonType, SuperSport.MatchCentre.UI.Web.SearchEngine.ButtonType.Image, StringComparison.OrdinalIgnoreCase))
           { %>
        <input id="btSearch" type="image" src="<%= ButtonSrc %>" />
        <% }
           else
           { %>
        <input id="btSearch" type="<%= ButtonType %>" value="<%= ButtonValue %>" />
        <% } %>
    </div>
    </form>
</div>