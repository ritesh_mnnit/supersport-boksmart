﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacebookLike.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.SocialNetwork.FacebookLike" EnableViewState="false" %>
<div  id="<%= ClientID %>" class="mc-ui-facebook-like-widget ui-widget ui-widget-content <%=CssClass %>">
    <div class="mc-ui-social-network-like"></div>
    <fb:like 
        href="<%= Href %>" 
        send="<%= Send %>" 
        layout="<%= Layout %>" 
        show_faces="<%= ShowFaces %>" 
        width="<%= Width %>" 
        height="<%= Height %>"
        action="<%= Action %>" 
        border_color="" 
        stream="false" 
        header="false"></fb:like>
    <br class="mc-ui-social-network-like-break" />
</div>