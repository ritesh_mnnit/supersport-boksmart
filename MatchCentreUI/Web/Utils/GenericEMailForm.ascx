﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericEMailForm.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Utils.GenericEMailForm" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-email-widget ui-widget ui-widget-content <%=CssClass %>">
    <h4>E-Mail &quot;<asp:Literal ID="LiteralSubject" runat="server"></asp:Literal>&quot;</h4>
    <asp:Panel ID="pnlEMailForm" runat="server">
<form id="formEmailForm" method="post" runat="server">
    <asp:HiddenField ID="hUrl" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hSubject" runat="server" ClientIDMode="Static" />
    <span class="mc-ui-input-row mc-ui-to-input-row">
        <asp:Label ID="lblTo" runat="server" Text="To:" AssociatedControlID="txtTo" CssClass="mc-ui-label"></asp:Label>
        <span>
            <asp:TextBox ID="txtTo" runat="server" CssClass="mc-ui-input" ClientIDMode="Static"></asp:TextBox>
            <asp:RequiredFieldValidator ID="valReqTo" runat="server" ErrorMessage=" (Required)" Text="" ControlToValidate="txtTo" CssClass="mc-ui-input-error mc-ui-req-input-error"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="valRegExTo" runat="server" ErrorMessage=" (Invalid)" Text="" ControlToValidate="txtTo" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="mc-ui-input-error mc-ui-regex-input-error"></asp:RegularExpressionValidator>
        </span>
    </span>

    <span class="mc-ui-input-row mc-ui-bcc-input-row">
        <asp:Label ID="lblBcc" runat="server" Text="Bcc:" AssociatedControlID="txtBcc" CssClass="mc-ui-label"></asp:Label>
        <span>
            <asp:TextBox ID="txtBcc" runat="server" CssClass="mc-ui-input" ClientIDMode="Static"></asp:TextBox>
        </span>
    </span>

    <span class="mc-ui-input-row mc-ui-from-input-row">
        <asp:Label ID="lblSender" runat="server" Text="Your e-mail:" AssociatedControlID="txtSender" CssClass="mc-ui-label"></asp:Label>
        <span>
            <asp:TextBox ID="txtSender" runat="server" CssClass="mc-ui-input" ClientIDMode="Static"></asp:TextBox>
            <asp:RequiredFieldValidator ID="valReqSender" runat="server" ErrorMessage=" (Required)" Text="" ControlToValidate="txtSender" CssClass="mc-ui-input-error mc-ui-req-input-error"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="valRegExSender" runat="server" ErrorMessage=" (Invalid)" Text="" ControlToValidate="txtSender" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="mc-ui-input-error mc-ui-regex-input-error"></asp:RegularExpressionValidator>
        </span>
    </span>

    <span class="mc-ui-input-row mc-ui-bcc-input-row">
        <asp:Label ID="lblCopyMe" runat="server" Text="" AssociatedControlID="chbCopyMe" CssClass="mc-ui-label"></asp:Label>
        <span>
            <asp:CheckBox ID="chbCopyMe" runat="server" Text="Copy Me" CssClass="mc-ui-input" ClientIDMode="Static" Checked="false"  />
        </span>
    </span>

    <span class="mc-ui-input-row mc-ui-message-input-row">
        <asp:Label ID="lblMessage" runat="server" Text="Message:" AssociatedControlID="txtMessage" CssClass="mc-ui-label"></asp:Label>
        <span>
            <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="5" CssClass="mc-ui-input" ClientIDMode="Static"></asp:TextBox>
        </span>
    </span>

    <span class="mc-ui-buttons-container">
        <span class="mc-ui-send-button-container mc-ui-button-container">
            <asp:Button ID="btnSend" runat="server" Text="Send" CssClass="mc-ui-submit-button mc-ui-button" CausesValidation="true" ClientIDMode="Static" />
        </span>

        <span class="mc-ui-cancel-button-container mc-ui-button-container">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="mc-ui-cancel-button mc-ui-button" CausesValidation="false" ClientIDMode="Static" />
        </span>
    </span>
</form>
    </asp:Panel>
    <asp:Panel ID="pnlEMailSuccess" runat="server" Visible="false">
        <span class="mc-ui-success-message">
            Your e-mail was sent successfully.
        </span>
        <script type="text/javascript" language="javascript">
            /* <![CDATA[ */

            (function($) {
                setTimeout("jQuery(document).trigger('close.facebox');", 3000);
            })(jQuery);

            /* ]]> */            
        </script>
    </asp:Panel>
    <asp:Panel ID="pnlEMailFailure" runat="server" Visible="false">
        <span class="mc-ui-error-message">
            There was an error sending your e-mail. Please try again later.
        </span>
        <script type="text/javascript" language="javascript">
            /* <![CDATA[ */

            (function($) {
                setTimeout("jQuery(document).trigger('close.facebox');", 10000);
            })(jQuery);

            /* ]]> */            
        </script>
    </asp:Panel>
</div>
