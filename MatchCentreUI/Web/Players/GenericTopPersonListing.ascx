﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericTopPersonListing.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Players.GenericTopPersonListing" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-top-person-widget ui-widget ui-widget-content <%=CssClass%>">
    <asp:ListView ID="rptTopPersons" runat="server" 
        onitemdatabound="rptTopPersons_ItemDataBound">
        <LayoutTemplate>
            <div id="divTopPersonHeader" class="mc-ui-widget-data-header" runat="server">
                <span class="mc-ui-top-person-name">Name</span>
                <span class="mc-ui-top-person-team">Team</span>
                <asp:ListView ID="rptTopPersonScoringEventsHeader" runat="server">
                    <LayoutTemplate>
                        <span id="itemPlaceholder" runat="server"></span>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <span id="spanTopPersonScoringEventHeader" class="mc-ui-top-person-scoring-event-<%# Container.DataItemIndex %>"><%# Container.DataItem %></span>
                    </ItemTemplate>
                </asp:ListView>
            </div>
            <div id="itemPlaceholder" runat="server"></div>
        </LayoutTemplate>
        <ItemTemplate>
            <div id="divTopPersonItemContainer" class="mc-ui-top-person-item-container" runat="server">
                <span class="mc-ui-top-person-name"><%# Eval("DisplayName")%></span>
                <span class="mc-ui-top-person-team"><%# Eval("TeamName")%></span>
                <asp:ListView ID="rptTopPersonScoringEvents" runat="server">
                    <LayoutTemplate>
                        <span id="itemPlaceholder" runat="server"></span>                        
                    </LayoutTemplate>
                    <ItemTemplate>
                        <span id="spanTopPersonScoringEvent" class="mc-ui-top-person-scoring-event-<%# Container.DataItemIndex %>"><%# Container.DataItem %></span>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ItemTemplate>
        <ItemSeparatorTemplate>
        </ItemSeparatorTemplate>
        <EmptyDataTemplate>
            <span class="mc-ui-empty-message">
            </span>
        </EmptyDataTemplate>
    </asp:ListView>
</div>
