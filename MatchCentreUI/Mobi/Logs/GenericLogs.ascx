﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericLogs.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Logs.GenericLogs" EnableViewState="false" %>
<asp:Repeater ID="rptLog" runat="server" onitemdatabound="rptLog_ItemDataBound">
    <HeaderTemplate>
<div class="<%=CssClass%>">    
    <div class="<%=CssClass%>Header">
        <span class="<%=CssClass%>HeaderPosition">&nbsp;</span>
        <span class="<%=CssClass%>HeaderTeam">Team</span>
        <span class="<%=CssClass%>HeaderPlayed">P</span>
        <span class="<%=CssClass%>HeaderWon">W</span>
        <span class="<%=CssClass%>HeaderDrew">D</span>
        <span class="<%=CssClass%>HeaderLost">L</span>
        <span class="<%=CssClass%>HeaderPts">Pts</span>
        <br class="<%=CssClass%>HeaderBreak" style="clear: both;" />
    </div>
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Panel ID="pnlGroup" runat="server" CssClass="<%=CssClass%>Header">
        <span><%# Eval("GroupName")%></span>
    </asp:Panel>
    <div class="<%=CssClass%>Item">
        <span class="<%=CssClass%>ItemPosition"><%# Eval("Position")%></span>
        <span class="<%=CssClass%>ItemTeam"><%# Eval("Team")%></span>
        <span class="<%=CssClass%>ItemPlayed"><%# Eval("Played")%></span>
        <span class="<%=CssClass%>ItemWon"><%# Eval("Won")%></span>
        <span class="<%=CssClass%>ItemDrew"><%# Eval("Drew")%></span>
        <span class="<%=CssClass%>ItemLost"><%# Eval("Lost")%></span>
        <span class="<%=CssClass%>ItemPts"><%# Eval("Points")%></span>
        <br class="<%=CssClass%>ItemBreak" style="clear: both;" />
    </div>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <asp:Panel ID="pnlGroup" runat="server" CssClass="<%=CssClass%>Header">
        <span><%# Eval("GroupName")%></span>
    </asp:Panel>
    <div class="<%=CssClass%>AltItem">
        <span class="<%=CssClass%>AltItemPosition"><%# Eval("Position")%></span>
        <span class="<%=CssClass%>AltItemTeam"><%# Eval("Team")%></span>
        <span class="<%=CssClass%>AltItemPlayed"><%# Eval("Played")%></span>
        <span class="<%=CssClass%>AltItemWon"><%# Eval("Won")%></span>
        <span class="<%=CssClass%>AltItemDrew"><%# Eval("Drew")%></span>
        <span class="<%=CssClass%>AltItemLost"><%# Eval("Lost")%></span>
        <span class="<%=CssClass%>AltItemPts"><%# Eval("Points")%></span>
        <br class="<%=CssClass%>AltItemBreak" style="clear: both;" />
    </div>
    </AlternatingItemTemplate>
    <SeparatorTemplate>
    </SeparatorTemplate>
    <FooterTemplate>
    <span class="<%=CssClass%>EmptyMessage">
        <asp:Literal ID="LiteralEmptyMessage" runat="server" Visible="false">
            There are currently no logs available.
        </asp:Literal>
    </span>
</div>
    </FooterTemplate>
</asp:Repeater>
