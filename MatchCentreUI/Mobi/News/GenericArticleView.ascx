﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericArticleView.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.News.GenericArticleView" EnableViewState="false" %>
<div class="<%=CssClass %>">
    <span class="<%=CssClass %>Headline">
        <asp:Literal ID="LiteralArticleHeadline" runat="server"></asp:Literal>
    </span>
    <br class="<%=CssClass %>HeadlineBreak" />
    <span class="<%=CssClass %>Timestamp">
        <asp:Literal ID="LiteralArticleTimestamp" runat="server"></asp:Literal>
    </span>
    <br class="<%=CssClass %>TimestampBreak" />
    <div class="<%=CssClass %>Body">
        <asp:Image ID="imgArticlePic" runat="server" CssClass="Image" />
        <%-- imgArticlePic.CssClass will be appended with GenericArticleView.CssClass property --%>
        <asp:Literal ID="LiteralArticleBody" runat="server"></asp:Literal>
        <br class="<%=CssClass %>BodyBreak" />
    </div>
    <div class="<%=CssClass %>SocialLinks">
        <asp:HyperLink ID="lnkTwitter" runat="server" Target="_blank">
            <img src="http://www.supersport.com/images/sm_twitter.png" alt="Twitter" title="Twitter" />
        </asp:HyperLink>
        <asp:HyperLink ID="lnkFacebook" runat="server" Target="_blank">
            <img src="http://www.supersport.com/images/sm_fb.png" alt="Facebook" title="Facebook" />
        </asp:HyperLink>
	</div>
    <div class="<%=CssClass %>SocialComments">
        <div id="fb-root"></div>
        <script>        
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            } (document, 'script', 'facebook-jssdk'));
        </script>
        <fb:comments href="<%= HttpContext.Current.Request.Url.AbsoluteUri %>"  num_posts="10" mobile="true"></fb:comments>    
    </div>
</div>
