﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericPhotoView.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Galleries.GenericPhotoView" EnableViewState="false" %>
<div class="<%=CssClass %>">
    <div class="<%=CssClass %>ImageContainer">
        <asp:Image ID="imgPic" runat="server" CssClass="Image" />
        <%-- imgPic.CssClass will be prepended with GenericPhotoView.CssClass property --%>
    </div>
    <br class="<%=CssClass %>ImageBreak" />
    <div class="<%=CssClass %>Body">
        <asp:Literal ID="LiteralBody" runat="server"></asp:Literal>
    </div>
    <br class="<%=CssClass %>BodyBreak" />
    <span class="<%=CssClass %>GalleryLinkContainer">
        <asp:HyperLink ID="lnkGallery" runat="server" CssClass="GalleryLink">
        <%-- lnkGallery.CssClass will be prepended with GenericPhotoView.CssClass property --%>
        back to gallery &gt;
        </asp:HyperLink>
    </span>
</div>
