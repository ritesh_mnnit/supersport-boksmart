﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericResultsList.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Results.GenericResultsList" EnableViewState="false" %>
<div class="<%=CssClass%>">
<asp:Repeater ID="rptResults" runat="server"
    onitemdatabound="rptResults_ItemDataBound">
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
    <div class="<%=CssClass%>Item">
        <span class="<%=CssClass%>ItemDate"><%# String.Format("{0:dd/MM/yy}",Eval("MatchDateTime"))%></span>
        <span class="<%=CssClass%>ItemMatch"><%# Eval("HomeTeam")%> <%# Eval("HomeTeamScore")%> &ndash; <%# Eval("AwayTeamScore")%> <%# Eval("AwayTeam")%></span>
        <span class="<%=CssClass%>ItemLeagueName">(<%# Eval("LeagueName")%>)</span>
        <br class="<%=CssClass%>ItemBreak" />
    </div>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <div class="<%=CssClass%>AltItem">
        <span class="<%=CssClass%>AltItemDate"><%# String.Format("{0:dd/MM/yy}",Eval("MatchDateTime"))%></span>
        <span class="<%=CssClass%>AltItemMatch"><%# Eval("HomeTeam")%> <%# Eval("HomeTeamScore")%> &ndash; <%# Eval("AwayTeamScore")%> <%# Eval("AwayTeam")%></span>
        <span class="<%=CssClass%>AltItemLeagueName">(<%# Eval("LeagueName")%>)</span>
        <br class="<%=CssClass%>AltItemBreak" />
    </div>
    </AlternatingItemTemplate>
    <SeparatorTemplate>
    </SeparatorTemplate>
    <FooterTemplate>
    <span class="<%=CssClass%>EmptyMessage">
        <asp:Literal ID="LiteralEmptyMessage" runat="server" Visible="false">
            There are currently no results available.
        </asp:Literal>
    </span>
    </FooterTemplate>
</asp:Repeater>
</div>
