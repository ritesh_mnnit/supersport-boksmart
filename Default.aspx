﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Bok.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="BokSmartWeb._Default" %>
<%@ Register src="Controls/CustomNewsBrowserBasic.ascx" tagPrefix="uc" tagName="CustomNewsBrowserBasic" %>
<%@ Register src="Controls/CustomGenericSiteContent.ascx" tagPrefix="uc" tagName="CustomGenericSiteContent" %>
<%@ Register src="Controls/CustomGenericNewsBrowser.ascx" tagPrefix="uc" tagName="CustomGenericNewsBrowser" %>
<%@ Register src="~/MatchCentreUI/Web/SocialNetwork/GenericSocialNetworkMetaTags.ascx" tagname="SocialNetworkMetaTags" tagprefix="mc" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <title>BOKSMART - Home</title>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-1.5.1.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery-ui-1.8.14.custom.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.ThreeDots.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/MatchCentreUI/scripts/jquery.dotdotdot-1.3.0.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/Flash/Script/swfobject.js") %>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <mc:SocialNetworkMetaTags 
        ID="mcSocialNetworkMetaTags" 
        runat="server"
        OpenGraphMetaType="<%# SuperSport.MatchCentre.UI.Web.SocialNetwork.OpenGraphMetaType.Website %>" />
    <div class="padding20"></div>
    <div class="spineline">
        <uc:CustomGenericSiteContent runat="server" ID="ucTopBannerAd"/>
    </div>
    <div class="padding20"></div>

    <div class="block620">
        <div class="featureheader">
            BOKSMART</div>
        <uc:CustomNewsBrowserBasic ID="NewsBrowser1" runat="server" CssClass="" Index="0" Count="4" />
        <%--<div class="block300_left">
            <a href="#">
                <img src="Images/feature_300x300.jpg" height="300" width="300" alt="Feature" border="0" /></a></div>
        <div class="block320_right">
            <div class="featureitem">
                <div class="padding10">
                </div>
                <div class="h2">
                    A News Article Heading Goes Here</div>
                <div class="padding5">
                    Lorem ipsum dolor sit amet, consectetuer...</div>
            </div>
            <div class="featureselected">
                <div class="padding10">
                </div>
                <div class="h3">
                    A News Article Heading Goes Here</div>
                <div class="padding5">
                    <div class="h4">
                        Lorem ipsum dolor sit amet, consectetuer...</div>
                </div>
            </div>
            <div class="featureitem">
                <div class="padding10">
                </div>
                <div class="h2">
                    A News Article Heading Goes Here</div>
                <div class="padding5">
                    Lorem ipsum dolor sit amet, consectetuer...</div>
            </div>
            <div class="featureitem">
                <div class="padding10">
                </div>
                <div class="h2">
                    A News Article Heading Goes Here</div>
                <div class="padding5">
                    Lorem ipsum dolor sit amet, consectetuer...</div>
            </div>
            <div class="featureitem">
                <div class="padding10">
                </div>
                <div class="h2">
                    A News Article Heading Goes Here</div>
                <div class="padding5">
                    Lorem ipsum dolor sit amet, consectetuer...</div>
            </div>
        </div>
        <div class="featurefooter" id="featureContainer">
            <div class="padding10">
            </div>
            <div class="featuremore">
                ...More news</div>
        </div>--%>

        <!-- Video & Facebook BEGIN -->
        <div class="block300_left">
            <div class="padding20"></div>
            <div class="LatestVideoHeader">
                Latest YouTube Video
            </div>
            <uc:CustomGenericSiteContent runat="server" ID="ucYoutubeChannel"/>
            <div class="BokSmartYouTubeLink">
                <a href="http://www.youtube.com/user/BOKSMARTSA/videos" target="_blank">BokSmart on Youtube</a>
            </div>
            <div class="padding20"></div>
            <uc:CustomGenericSiteContent runat="server" ID="ucFacebookWidget"/>
            <div class="padding20"></div>
        </div>
        <!-- Video & Facebook END -->
    
        <!-- Twitter BEGIN -->
        <div class="block300">
            <div class="padding20"></div>
            <iframe SRC="/SocialMedia/TwitterWidget.aspx" WIDTH=300 HEIGHT=600 frameborder="0" scrolling="no"></iframe>
        </div>
        <!-- Twitter END -->
    </div>

    <!-- Right BEGIN -->
    <%--<div class="block300_right">
        <uc:CustomGenericSiteContent runat="server" CssClass="" ID="ucRightAd1" />
        <div class="padding20"></div>
        <!-- News BEGIN -->
        <div class="news">
    	    <div class="header">NEWS HEADLINES</div>
            <uc:CustomGenericNewsBrowser ID="ucSaRugbyNews" runat="server" />
            <div class="archive"><a href="http://www.sarugby.co.za/newsarchive.aspx?category=sarugby">...News archive</a></div>
            <div class="padding20"></div>
        </div>
        <!-- NEWS END -->
    
        <!-- Ad Space BEGIN -->
        <div class="padding20"></div>
        <uc:CustomGenericSiteContent runat="server" CssClass="" ID="ucRightAd2"/>
        <div class="padding20"></div>
        <uc:CustomGenericSiteContent runat="server" CssClass="" ID="ucRightAd3"/>
        <div class="padding20"></div>
        <uc:CustomGenericSiteContent runat="server" CssClass="" ID="ucRightAd4"/>
        <div class="padding20"></div>
        <uc:CustomGenericSiteContent runat="server" CssClass="" ID="ucRightAd5"/>
        <div class="padding20"></div>
        <uc:CustomGenericSiteContent runat="server" CssClass="" ID="ucRightAd6"/>
        <!-- Ad Space END -->
        <div class="padding20"></div>
    </div>--%>

    <!-- Right END -->
</asp:Content>
