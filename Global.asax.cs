﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.MatchCentre.UI.Utils.Web;

namespace BokSmartWeb
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            LocalRoutingHelper.BuildNewsArchiveRouting("news/archive/", "~/news/archive.aspx");
            LocalRoutingHelper.BuildDisclaimerRouting("disclaimer", "~/disclaimer.aspx");
            RoutingHelper.BuildNewsViewRouting("news/article/", "~/news/article.aspx");
            RoutingHelper.BuildNewsBrowseRouting("news/", "~/news/archive.aspx");
            LocalRoutingHelper.BuildPressReleaseRouting("press/", "~/news/archive.aspx");
            RoutingHelper.BuildGalleriesRouting("galleries/", "~/media/photo/gallerylist.aspx");
            RoutingHelper.BuildGalleryRouting("gallery/", "~/media/photo/Gallery.aspx");
            LocalRoutingHelper.BuildContentRouting("content/{title}", "~/content/viewcontent.aspx");
            LocalRoutingHelper.BuildPdfContentRouting("content/PATH_TO_BOKSMART_PDF_DOC/{doc}", "~/content/viewcontent.aspx");
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
